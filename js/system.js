
/**
 * .btnOnClickDisable
 * Once button is clicked it gets disabled. To avoid buttons to be ran twice.
 */
$(document).on("click", ".btnOnClickDisable", function(){
    var button = $(this);
    $(this).closest("form").on("submit", function(){
        $(this).find("input[name='action']").val($(button).val());
        $(button).prop("disabled", true);
    });
});

$(function(){
    
});