

function loadCart(show) {

    // Sets up Table
    $("#modal-store-table-body").html("<tr><td colspan='5' style='text-align: center;'><img src='./panel/img/loading.gif' style='width: 35px;'></td></tr>");

    // Show Modal
    if (show == true) {
        $("#modal-store").modal("show");
    }

    $.post("./php/ajax/ajax.load.cart.php", function (r) {

        // Sets up Table
        $("#modal-store-table-body").html(r);

    });
}

function loadItemsNumber() {
    $.post("./php/ajax/ajax.get.cart.items.number.php", function (r) {

        // Sets up Table
        $(".cart-items-number").html(r);

    });
}

$(function () {

    loadItemsNumber();
    loadCart(false);

    $(document).on("click", ".btnOpenCart", function () {
        loadCart(true);
    });

    $(document).on("keyup", ".txtQuantity", function () {
        var quantity = $(this).val();
        $(this).val(isNaN(quantity) ? 0 : parseInt(quantity, 10));
    });

    $(document).on("click", ".BtnRemoveItem", function () {
        var button = $(this);
        var position = $(this).attr("position");

        if (confirm("Do you wish to proceed?")) {
            $.post("./php/ajax/ajax.remove.from.cart.php", {
                position: position
            }, function (r) {
                loadItemsNumber();
                $(button).closest("tr").fadeOut();
            });
        }
    });

    $(document).on("click", ".btnMinus", function () {
        var id = $(this).attr("refid");
        var quantity = $(id).val();
        quantity = parseInt(quantity);
        $(id).val((quantity > 0) ? (quantity - 1) : 0)
    });

    $(document).on("click", ".btnPlus", function () {
        var id = $(this).attr("refid");
        var quantity = $(id).val();
        quantity = parseInt(quantity);
        $(id).val((quantity < 99) ? (parseInt(quantity) + 1) : quantity);
    });

    $(document).on("change", ".cmbVariant", function () {
        var item = $(this).attr("item");
        var selected = $(this).find("option:selected");
        var variant = $(selected).val();
        var price = $(selected).attr("price");
        var instock = $(selected).attr("instock") == 1 ? "In Stock" : "Out of Stock";

        $(".store-item-price[item='" + item + "']").html(instock + " - $ " + price);
    });

    $(document).on("click", ".btnAddToCart", function () {
        var item = $(this).attr("item");

        var quantity = $(".txtQuantity[item='" + item + "']").val();
        if (quantity > 0) {

            $.post("./php/ajax/ajax.add.to.cart.php", {
                item: item,
                quantity: quantity
            }, function (r) {

                // Setting quantity field value as zero
                $(quantity).val(0);

                loadItemsNumber();
                loadCart(true);
            });

        }
    });
});