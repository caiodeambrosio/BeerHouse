
function maskPrice() {
    $('.fldPrice').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
}
function maskDateCustom() {
    // It was moved to head.php because of PHP
    $(".fldDate").datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        changeMonth: true,
        changeYear: true
    });
}
function maskTime() {
    $(".fldTime").mask("99:99");
}
function maskPhone() {
    $(".fldPhone").mask("(999) 999-9999");
}
function maskExpDate() {
    $(".fldExpDate").mask("00/0000");
}
function maskCardNumber() {
    $(".fldCard").mask("0000 0000 0000 0000 0000");
}

$(function () {

    maskPrice();
    maskTime();
    maskPhone();
    maskExpDate();
    maskCardNumber();

});