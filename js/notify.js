
function Notify (){
    
    this.NOTIFY_TYPE_ERROR = 1;
    this.NOTIFY_TYPE_SUCCESS = 2;
    this.NOTIFY_TYPE_INFO = 3;
    this.NOTIFY_TYPE_WARNING = 4;
    
    this.notify = function (id, title, message, type){

        var modalClass = "";
        var notification = "";

        switch(type){
            case NOTIFY_TYPE_ERROR:{
                modalClass = "modal-danger";
            } break;
            case NOTIFY_TYPE_SUCCESS:{
                modalClass = "modal-success";
            } break;
            case NOTIFY_TYPE_INFO:{
                modalClass = "modal-info";
            } break;
            case NOTIFY_TYPE_WARNING:{
                modalClass = "modal-warning";
            } break;
        }

        notification += "<div class='modal " + modalClass + "'>";
        notification += ("<b>" + title + "</b> " + message);
        notification += "</div>";

        $("#" + id).append(notification);
    }
}
