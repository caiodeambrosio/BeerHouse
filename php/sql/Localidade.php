<?php

class Localidade {

    function __construct() {
        
    }

    public static function validate($localidades) {
        global $notify;
        if (empty($localidades["pll_custo_envio"])) {
            $notify->set("Erro", "Todos os Campos <b>Custo de Envio</b> São Obrigatórios.", Notify::NOTIFY_TYPE_ERROR);
        } else {
            return true;
        }
        return false;
    }

    public static function saveLocalidade($id, $localidade) {
        global $database;

        $query = "INSERT tbl_propriedades_loja_localidade ";
        $query .= "(";
        $query .= " pll_id_pub, ";
        $query .= " pll_id_propriedades_loja, ";
        $query .= " pll_nome, ";
        $query .= " pll_custo_envio ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '" . Session::get("pub")["pub_id"] . "', ";
        $query .= " '{$id}', ";
        $query .= " '{$localidade["pll_nome"]}', ";
        $query .= " '{$localidade["pll_custo_envio"]}' ";
        $query .= ") ";

        $database->query($query);
        return $database->insertId();
    }

    public static function getLocalidade($id) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_propriedades_loja_localidade ";
        $query .= "WHERE pll_id = {$id} ";

        return $database->getOne($query);
    }

    public static function getLocalidades($where = "") {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_propriedades_loja_localidade ";
        $query .= "WHERE pll_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        if (!empty($where)) {
            $query .= "AND {$where} ";
        }
        $query .= "ORDER BY pll_nome DESC ";


        return $database->selectAll($query);
    }

    public static function updateLocalidade($localidade) {        
        global $database;

        $query = "UPDATE tbl_propriedades_loja_localidade ";
        $query .= "SET ";
        $query .= "pll_nome = '{$localidade["pll_nome"]}', ";
        $query .= "pll_custo_envio = '{$localidade["pll_custo_envio"]}' ";
        echo $query .= "WHERE pll_id = '{$localidade["pll_id"]}';";
        
        die;
        return $database->update($query) == 1 ? true : false;
    }

}
