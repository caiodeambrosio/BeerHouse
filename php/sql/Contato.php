<?php

class Contato {

    function __construct() {
        
    }

    public static function validate($contato) {
        global $notify;

        if (empty($contato["con_nome"])) {
            $notify->set("Erro", "Campo <b>Nome</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "formContato");
        } else if (empty($contato["con_email"])) {
            $notify->set("Erro", "Campo <b>Email</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "formContato");
        } else if (empty($contato["con_telefone"])) {
            $notify->set("Erro", "Campo <b>Telefone</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "formContato");
        } else if (empty($contato["con_assunto"])) {
            $notify->set("Erro", "Campo <b>Assunto</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "formContato");
        } else if (empty($contato["con_mensagem"])) {
            $notify->set("Erro", "Campo <b>Mensagem</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "formContato");
        } else {
            return true;
        }
        return false;
    }

    public static function validateResposta($contato) {
        global $notify;

        if (empty($contato["con_resposta"])) {
            $notify->set("Erro", "Campo <b>Resposta</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function totalContatos($where) {
        global $database;

        $query = "SELECT COUNT(*) AS total ";
        $query .= "FROM tbl_con_contato ";
        $query .= "WHERE con_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        $query .= "AND {$where} ";

        return $database->getOne($query);
    }
    
    public static function saveContato($contato) {
        global $database;

        $query = "INSERT INTO tbl_con_contato ";
        $query .= "(";
        $query .= " con_id_pub, ";
        $query .= " con_nome, ";
        $query .= " con_email, ";
        $query .= " con_telefone, ";
        $query .= " con_assunto, ";
        $query .= " con_mensagem ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '". Session::get("pub")["pub_id"] ."', ";
        $query .= " '{$contato["con_nome"]}', ";
        $query .= " '{$contato["con_email"]}', ";
        $query .= " '{$contato["con_telefone"]}', ";
        $query .= " '{$contato["con_assunto"]}', ";
        $query .= " '{$contato["con_mensagem"]}' ";
        $query .= ")";

        $database->query($query);
        return $database->insertId();
    }

    public static function getContato($id) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_con_contato ";
        $query .= "WHERE con_id = {$id} ";

        return $database->getOne($query);
    }

    public static function getContatos($where = "") {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_con_contato ";
        $query .= "WHERE con_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        if (!empty($where)) {
            $query .= "AND {$where} ";
        }
        $query .= "ORDER BY con_status ASC, con_data_criacao DESC ";
         

        return $database->selectAll($query);
    }

    public static function updateContato($contato) {

        global $database;

        $query = "UPDATE tbl_con_contato ";
        $query .= "SET ";
        $query .= "con_resposta = '{$contato["con_resposta"]}', ";
        $query .= "con_respondido = '1', ";
        $query .= "WHERE ";
        $query .= "con_id = '{$contato["con_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

}
