<?php

class ItemLoja {

    function __construct() {
        
    }

    public static function validateSaveItemLoja($itemLoja) {
        global $notify;

        if (empty($itemLoja["itl_titulo"])) {
            $notify->set("Erro: ", "Campo <b>Título</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($itemLoja["itl_descricao"])) {
            $notify->set("Erro: ", "Campo <b>Descrição</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($itemLoja["itl_valor"])) {
            $notify->set("Erro: ", "Campo <b>Valor</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function saveItemLoja($itemLoja, $imagem = array()) {
        global $database;

        $query = "INSERT INTO tbl_itl_item_loja ";
        $query .= "(";
        $query .= " itl_id_pub, ";
        $query .= " itl_titulo, ";
        $query .= " itl_descricao, ";
        $query .= " itl_valor, ";
        $query .= " itl_status ";
        if (count($imagem) == 1) {
            $query .= ", ";
            $query .= " itl_imagem ";
        }
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '". Session::get("pub")["pub_id"] ."', ";
        $query .= " '{$itemLoja["itl_titulo"]}', ";
        $query .= " '{$itemLoja["itl_descricao"]}', ";
        $query .= " '{$itemLoja["itl_valor"]}', ";
        $query .= " '{$itemLoja["itl_status"]}' ";
        if (count($imagem) == 1) {
            $query .= ", ";
            $query .= " '{$imagem[0]["unique"]}' ";
        }
        $query .= ")";
        
        $database->query($query);
        return $database->insertId();
    }
    
    public static function updateItemLoja($itemLoja) {
        global $database;

        $query = "UPDATE tbl_itl_item_loja ";
        $query .= "SET ";
        $query .= "itl_titulo = '{$itemLoja["itl_titulo"]}', ";
        $query .= "itl_descricao = '{$itemLoja["itl_descricao"]}', ";
        $query .= "itl_valor = '{$itemLoja["itl_valor"]}', ";
        $query .= "itl_status = '{$itemLoja["itl_status"]}' ";
        $query .= "WHERE ";
        $query .= "itl_id = '{$itemLoja["itl_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function getItemLoja($id) {
        global $database;
        
        $query = "SELECT * ";
        $query .= "FROM tbl_itl_item_loja ";
        $query .= "WHERE ";
        $query .= "itl_id = '{$id}';";

        return $database->getOne($query);
    }

    public static function getItensLoja() {
        global $database;
        
        $query = "SELECT * ";
        $query .= "FROM tbl_itl_item_loja ";
        $query .= "WHERE itl_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        $query .= "ORDER BY itl_titulo";

        return $database->selectAll($query);
    }

    

}
