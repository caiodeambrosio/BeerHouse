<?php

class Pub {

    function __construct() {
        
    }

    public static function validateSavePub($pub, $logo = array()) {
        global $notify;
        if (empty($pub["pub_titulo"])) {
            $notify->set("Erro: ", "Campo <b>Título</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_descricao"])) {
            $notify->set("Erro: ", "Campo <b>Descrição</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_website"])) {
            $notify->set("Erro: ", "Campo <b>Website</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function validateUpdatePub($pub, $logo = array(), $logo_old = array()) {
        global $notify;
        if (empty($pub["pub_titulo"])) {
            $notify->set("Erro: ", "Campo <b>Título</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_descricao"])) {
            $notify->set("Erro: ", "Campo <b>Descrição</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_website"])) {
            $notify->set("Erro: ", "Campo <b>Website</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($pub["pub_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function savePub($pub, $logo) {
        global $database;
        $query = "INSERT INTO tbl_pub_pub ";
        $query .= "(";
        $query .= " pub_titulo, ";
        $query .= " pub_descricao, ";
        $query .= " pub_website, ";
        $query .= " pub_email, ";
        $query .= " pub_facebook, ";
        $query .= " pub_twitter, ";
        $query .= " pub_instagram, ";
        $query .= " pub_youtube, ";
        $query .= " pub_texto_botao, ";
        $query .= " pub_link_botao, ";
        $query .= " pub_exibir_botao, ";
        $query .= " pub_status ";
        if (count($logo) == 1) {
            $query .= ", ";
            $query .= " pub_logo ";
        }
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '{$pub["pub_titulo"]}', ";
        $query .= " '{$pub["pub_descricao"]}', ";
        $query .= " '{$pub["pub_website"]}', ";
        $query .= " '{$pub["pub_email"]}', ";
        $query .= " '{$pub["pub_facebook"]}', ";
        $query .= " '{$pub["pub_twitter"]}', ";
        $query .= " '{$pub["pub_instagram"]}', ";
        $query .= " '{$pub["pub_youtube"]}', ";
        $query .= " '{$pub["pub_texto_botao"]}', ";
        $query .= " '{$pub["pub_link_botao"]}', ";
        $query .= " '{$pub["pub_exibir_botao"]}', ";
        $query .= " '{$pub["pub_status"]}' ";
        if (count($logo) == 1) {
            $query .= ", ";
            $query .= " '{$logo[0]["unique"]}' ";
        }
        $query .= ")";

        $database->query($query);
        $pub_id = $database->insertId();

        $query = "INSERT INTO tbl_adp_admin_pub ";
        $query .= "(";
        $query .= " adp_id_admin, ";
        $query .= " adp_id_pub ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '" . Session::get("admin")["adm_id"] . "', ";
        $query .= " '" . $pub_id . "' ";
        $query .= ")";

        $database->query($query);

        return $pub_id;
    }

    public static function updatePub($pub, $logo = array(), $banner = array()) {
        global $database;

        $query = "UPDATE tbl_pub_pub ";
        $query .= "SET ";
        $query .= "pub_titulo = '{$pub["pub_titulo"]}', ";
        $query .= "pub_descricao = '{$pub["pub_descricao"]}', ";
        $query .= "pub_website = '{$pub["pub_website"]}', ";
        $query .= "pub_email = '{$pub["pub_email"]}', ";
        $query .= "pub_facebook = '{$pub["pub_facebook"]}', ";
        $query .= "pub_twitter = '{$pub["pub_twitter"]}', ";
        $query .= "pub_instagram = '{$pub["pub_instagram"]}', ";
        $query .= "pub_youtube = '{$pub["pub_youtube"]}', ";
        $query .= "pub_exibir_botao = '{$pub["pub_exibir_botao"]}', ";
        $query .= "pub_status = '{$pub["pub_status"]}' ";
        if (isset($pub["pub_texto_botao"])) {
            $query .= ",";
            $query .= "pub_texto_botao = '{$pub["pub_texto_botao"]}' ";
        }
        if (isset($pub["pub_link_botao"])) {
            $query .= ",";
            $query .= "pub_link_botao = '{$pub["pub_link_botao"]}' ";
        }
        if (count($logo) == 1) {
            $query .= ", ";
            $query .= "pub_logo = '{$logo[0]["unique"]}' ";
        }
        if (count($pub) == 1) {
            $query .= ", ";
            $query .= "pub_banner = '{$banner[0]["unique"]}' ";
        }
        $query .= "WHERE ";
        $query .= "pub_id = '{$pub["pub_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function getPub($id) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_pub_pub ";
        $query .= "WHERE pub_id = {$id}";
        return $database->getOne($query);
    }

    public static function getPubs() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_pub_pub ";
        $query .= "ORDER BY pub_titulo";

        return $database->selectAll($query);
    }

    public static function getPubsByOwner() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_pub_pub ";
        $query .= "LEFT JOIN tbl_adp_admin_pub ";
        $query .= "ON tbl_pub_pub.pub_id = tbl_adp_admin_pub.adp_id_pub ";
        $query .= "WHERE adp_id_admin = '" . Session::get("admin")["adm_id"] . "' ";
        $query .= "ORDER BY pub_titulo ";

        return $database->selectAll($query);
    }

}
