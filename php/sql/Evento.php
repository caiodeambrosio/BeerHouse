<?php

class Evento {

    function __construct() {
        
    }

    public static function validateSaveEvento($evento) {
        global $notify;

        if (empty($evento["eve_titulo"])) {
            $notify->set("Erro: ", "Campo <b>Título</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_descricao"])) {
            $notify->set("Erro: ", "Campo <b>Descrição</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_data"])) {
            $notify->set("Erro: ", "Campo <b>Data</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_hora_inicio"])) {
            $notify->set("Erro: ", "Campo <b>Hora Inicio</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        }else if (empty($evento["eve_hora_fim"])) {
            $notify->set("Erro: ", "Campo <b>Hora Fim</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        }else {
            return true;
        }
        return false;
    }
    
    public static function validateUpdateEvento($evento) {
        global $notify;

        if (empty($evento["eve_titulo"])) {
            $notify->set("Erro: ", "Campo <b>Título</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_descricao"])) {
            $notify->set("Erro: ", "Campo <b>Descrição</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_data"])) {
            $notify->set("Erro: ", "Campo <b>Data</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($evento["eve_hora_inicio"])) {
            $notify->set("Erro: ", "Campo <b>Hora Inicio</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        }else if (empty($evento["eve_hora_fim"])) {
            $notify->set("Erro: ", "Campo <b>Hora Fim</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        }else {
            return true;
        }
        return false;
    }

    public static function saveEvento($evento, $banner = array()) {
        global $database;

        $query = "INSERT INTO tbl_eve_evento ";
        $query .= "(";
        $query .= " eve_titulo, ";
        $query .= " eve_descricao, ";
        $query .= " eve_valor, ";
        $query .= " eve_data, ";
        $query .= " eve_hora_inicio, ";
        $query .= " eve_hora_fim, ";
        $query .= " eve_id_pub, ";
        $query .= " eve_status ";
        if (count($banner) == 1) {
            $query .= ", ";
            $query .= " eve_banner ";
        }
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '{$evento["eve_titulo"]}', ";
        $query .= " '{$evento["eve_descricao"]}', ";
        $query .= " '{$evento["eve_valor"]}', ";
        $query .= " '{$evento["eve_data"]}', ";
        $query .= " '{$evento["eve_hora_inicio"]}', ";
        $query .= " '{$evento["eve_hora_fim"]}', ";
        $query .= " '". Session::get("pub")["pub_id"] ."', ";
        $query .= " '{$evento["eve_status"]}' ";
        
        if (count($banner) == 1) {
            $query .= ", ";
            $query .= " '{$banner[0]["unique"]}' ";
        }
        echo $query .= ")";
        
        $database->query($query);
        return $database->insertId();
    }
    
    public static function updateEvento($evento) {
        global $database;

        $query = "UPDATE tbl_eve_evento ";
        $query .= "SET ";
        $query .= "eve_titulo = '{$evento["eve_titulo"]}', ";
        $query .= "eve_descricao = '{$evento["eve_descricao"]}', ";
        $query .= "eve_valor = '{$evento["eve_valor"]}', ";
        $query .= "eve_data = '{$evento["eve_data"]}', ";
        $query .= "eve_hora_inicio = '{$evento["eve_hora_inicio"]}', ";
        $query .= "eve_hora_fim = '{$evento["eve_hora_fim"]}', ";
        $query .= "eve_status = '{$evento["eve_status"]}' ";
        $query .= "WHERE ";
        $query .= "eve_id = '{$evento["eve_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function getEvento($id) {
        global $database;
        
        $query = "SELECT * ";
        $query .= "FROM tbl_eve_evento ";
        $query .= "WHERE ";
        $query .= "eve_id = '{$id}';";

        return $database->getOne($query);
    }

    public static function getEventos() {
        global $database;
        
        $query = "SELECT * ";
        $query .= "FROM tbl_eve_evento ";
        $query .= "WHERE eve_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        $query .= "ORDER BY eve_titulo";

        return $database->selectAll($query);
    }

    

}
