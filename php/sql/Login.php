<?php

class Login {
    
    function __construct(){
        
    }
    
    public static function logged(){
        $admin = Session::get("admin");
        return ($admin != null)? true : false;
    }
    
    public static function login($username, $password){
        $admin = Admin::getAdminByLogin($username, $password);
        if($admin != null){
            Session::set("admin", $admin);
            return true;
        }
        return false;
    }
    
    public static function logout(){
        Session::un_set("admin");
    }
}
