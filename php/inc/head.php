<?php
$head_title = !empty($head_title) ? $head_title : "BEER HOUSE";
?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $head_title?></title>
<link rel="icon" href="favicon.ico">

<link type="text/css" rel="stylesheet" href="lib/font-awesome/css/font-awesome.css" />
<link type="text/css" rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css" />
<link type="text/css" rel="stylesheet" href="lib/jquery.bootstrap/css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="lib/jquery.bootstrap/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="lib/jquery.ui/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/agency.css"/>
<!--<link type="text/css" rel="stylesheet" href="css/agency.min.css"/>-->
<link type="text/css" rel="stylesheet" href="css/template.css"/>

<script type="text/javascript" src="lib/jquery/jquery.js"></script>
<script type="text/javascript" src="lib/jquery.ui/jquery-ui.js"></script>
<script type="text/javascript" src="lib/jquery.easing/jquery.easing.js"></script>
<script type="text/javascript" src="lib/jquery.popper/popper.min.js"></script>
<script type="text/javascript" src="lib/jquery.bootstrap/js/tether.min.js"></script>
<script type="text/javascript" src="lib/jquery.bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="lib/jquery.bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="lib/jquery.maskMoney/jquery.maskMoney.js"></script>
<script type="text/javascript" src="lib/jquery.priceFormat/jquery.priceformat.js"></script>
<script type="text/javascript" src="lib/jquery.maskedInput/jquery.maskedInput.js"></script>
<script type="text/javascript" src="lib/jquery.smoothScroll/jquery.smoothScroll.js"></script>


<script type="text/javascript" src="js/masks.js"></script>
<script type="text/javascript" src="js/notify.js"></script>
<script type="text/javascript" src="js/system.js"></script>

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>