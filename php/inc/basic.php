<?php

require_once ("./php/class/Database.class.php");
require_once ("./php/class/Notify.class.php");
require_once ("./php/class/Session.class.php");
require_once ("./php/class/SystemMessage.class.php");
require_once ("./php/class/Util.class.php");

require_once ("./php/sql/ItemLoja.php");
require_once ("./php/sql/PropriedadesLoja.php");
require_once ("./php/sql/Pub.php");
require_once ("./php/sql/Evento.php");
require_once ("./php/sql/Localidade.php");

Session::start();
$database = new Database();
$notify = new Notify();

// Getting Pub by Website Domain
$domain = Util::isOnline() ? $_SERVER["HTTP_HOST"] : $_SERVER["REQUEST_URI"];
$pubs = Pub::getPubs();

for ($i = 0; $i < count($pubs); $i++) {
    if (in_array($pubs[$i]["pub_dominio"], preg_split("/[.\/]/", $domain))) {
        Session::set("pub", $pubs[$i]);
    }
}

?>