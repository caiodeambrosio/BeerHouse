
<?php
if(Session::is_set("user")){
?>

<div class="container" id="links2" style="margin-bottom: 10px;">

    <div class="row">
        <div class="col-lg-12">

            <nav class="navbar navbar-inverse navbar-toggleable-md" style="background-color: #50A6CF; border-radius: 0 0 6px 6px;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown2" aria-controls="navbarNavDropdown2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <a class="navbar-brand" href="panel.dashboard.php"><span class="fa fa-th-large"></span></a>

                <div class="collapse navbar-collapse" id="navbarNavDropdown2">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="panel.dashboard.php" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-th-large"></span> Dashboard</a>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                
                                <a class="dropdown-item" href="panel.dashboard.php"><span class="fa fa-th-large"></span> Dashboard</a>
                                <a class="dropdown-item" href="panel.alerts.php"><span class="fa fa-bell"></span> Alerts</a>
                                <a class="dropdown-item" href="panel.register.php"><span class="fa fa-soccer-ball-o"></span> Register Now</a>
                                <a class="dropdown-item" href="store.php"><span class="fa fa-shopping-cart"></span> Store</a>
                                <a class="dropdown-item" href="panel.players.php"><span class="fa fa-users"></span> My Players</a>
                                <a class="dropdown-item" href="panel.payments.php"><span class="fa fa-dollar"></span> Payments</a>
                                <a class="dropdown-item" href="panel.waivers.php"><span class="fa fa-files-o"></span> Waivers</a>
                                <a class="dropdown-item" href="panel.account.php"><span class="fa fa-user"></span> My Account</a>
                                <a class="dropdown-item" href="panel.accounts.php"><span class="fa fa-address-card-o"></span> Accounts</a>
                                <a class="dropdown-item" href="panel.password.php"><span class="fa fa-key"></span> Change Password</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Hi, <?= Session::get("user")["usr_first_name"] ?> <?= Session::get("user")["usr_last_name"] ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="panel.account.php"><span class="fa fa-user"></span> My Account</a>
                                <a class="dropdown-item" href="panel.password.php"><span class="fa fa-key"></span> Change Password</a>
                                <a class="dropdown-item" href="panel.signout.php"><span class="fa fa-sign-out"></span> Sign Out</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

        </div>
        
    </div>

</div>

<?php
}
?>