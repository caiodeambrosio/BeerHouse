
<?php
$eventos = Evento::getEventos("eve_active = 1");
if (!empty($eventos)) {
    ?>

    <div id="MyCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            for ($i = 0; $i < count($eventos); $i++) {
                ?>
                <li data-target="#MyCarousel" data-slide-to="<?= $i ?>" class="<?= $i == 0 ? "active" : "" ?>"></li>
                <?php
            }
            ?>
        </ol>
        <div class="carousel-inner" role="listbox">

            <?php
            for ($i = 0; $i < count($eventos); $i++) {
                ?>
                <div class="carousel-item <?= $i == 0 ? "active" : "" ?>">
                    <a href="<?= !empty($eventos[$i]["eve_link"]) ? $eventos[$i]["eve_link"] : "javascript:void(0);" ?>" <?= (empty($eventos[$i]["eve_link"])) ? "" : "target='_blank'" ?>>
                        <div class="row justify-content-md-center">
                            <img class="d-block box-evento-banner" src="./img/repo/<?= $eventos[$i]["eve_banner"] ?>">  
                            <div class="carousel-caption banner-title">
                                <h3><?= $eventos[$i]["eve_titulo"] ?></h3>
                                <p><?= $eventos[$i]["eve_descricao"] ?></p>
                                <?php !empty($eventos[$i]["eve_valor"]) ? "<p>{$eventos[$i]["eve_valor"]}</p>" : ""; ?>
                                
                            </div>
                        </div>
                    </a>

                </div>
                <?php
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#MyCarousel" role="button" data-slide="prev">
            <span class="fa fa-chevron-left"  style="color: black;"  aria-hidden="true"></span>

            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#MyCarousel" role="button" data-slide="next">
            <span class="fa fa-chevron-right" style="color: black;" aria-hidden="true"></span>

            <span class="sr-only">Next</span>
        </a>
    </div>

    <?php
}
?>
