<?php

class Database extends PDO {

    private $conexao = null;
    private $HOST = null;
    private $USER = null;
    private $PASSWORD = null;
    private $DATABASE = null;

    public function __construct() {
        $this->DATABASE = "redrockpub";
        $this->HOST = "localhost";
        $this->USER = "redrockpub_user";
        $this->PASSWORD = "admin";
        $this->conexao = new PDO("mysql:dbname={$this->DATABASE};host={$this->HOST}", $this->USER, $this->PASSWORD);
    }

    public function setParams($statement, $parameters = array()) {
        foreach ($parameters as $key => $value) {
            $this->setParam($key, $value);
        }
    }

    public function setParam($statement, $key, $value) {
        $statement->bindParam($key, $value);
    }

    public function query($query, $params = array()) {
        $stmt = $this->conexao->prepare($query);
        $this->setParams($stmt, $params);
        $stmt->execute();
        
        return $stmt;
    }

    public function selectAll($query): array {
        $stmt = $this->query($query);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOne($query) {
        $stmt = $this->query($query);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function insertId() {
        $lastInsertId = $this->conexao->lastInsertId();
        return  is_numeric($lastInsertId) ? $lastInsertId : null;
    }

    public function update($query) {
        $stmt = $this->query($query);
        
        return $stmt->rowCount();
    }

}
