<?php

class Util{
    
    public function __construct(){
        
    }
    
    public static function debug($data){
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public static function generatePassword(){
        return sprintf("%04d", rand(0,9999));
    }
    
    public static function getDateToDatabase($date){
        $array = explode("/", $date);
        return "{$array[2]}/{$array[1]}/{$array[0]}";
    }
    
    public static function getDateToShow($date){
        $array = explode("-", $date);        
        return "{$array[2]}/{$array[1]}/{$array[0]}";
    }
    
    public static function getDateTimeToShow($date){
        $array = explode(" ", $date);        
        $arrayDate = explode("-", $array[0]);        
        $arrayTime = $array[1];        
        return "{$arrayDate[2]}/{$arrayDate[1]}/{$arrayDate[0]} - {$arrayTime}";
    }
    
    public static function getTimeToShow($time){
        $array = explode(":", $time);
        return "{$array[0]}:{$array[1]}";
    }
    
    public static function getValueToSave($value){
        $value = str_replace("R$","", $value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",",".", $value);
        return $value;
    }
    
    public static function getUniqueName(){
        return uniqid('', true);
    }
    
    public static function getCurrentPage(){
        return @end(explode("/", $_SERVER["SCRIPT_NAME"]));
    }
    
    public static function isOnline(){
        return (strpos($_SERVER['DOCUMENT_ROOT'], "beerhouse") !== FALSE)? true : false;
    }
}

?>