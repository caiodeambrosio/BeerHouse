<?php

class Upload {

    function __construct() {
        
    }

    /**
     * 
     * @param $name Name of the input file that is being processed
     * @param $directory Directory where the file is going to be uploaded to
     * @param $unique Generates a unique name for the file
     */
    public static function upload($name, $directory, $unique = false) {

        $file_names = array();

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }
        
        if (isset($_FILES[$name])) {
            if (!is_array($_FILES[$name]["name"]) && !empty($_FILES[$name]["name"])) {
                $extension = pathinfo($_FILES[$name]["name"], PATHINFO_EXTENSION);
                $uniqueName = $unique ? Util::getUniqueName() . "." . $extension : "";
                move_uploaded_file($_FILES[$name]["tmp_name"], $directory . ($unique ? $uniqueName : $_FILES[$name]["name"]));
                array_push($file_names, array(
                    "original" => $_FILES[$name]["name"],
                    "unique" => $uniqueName
                ));
            } else if (is_array($_FILES[$name]["name"]) && !empty($_FILES[$name]["name"][0])) {
                for ($i = 0; $i < count($_FILES[$name]["name"]); $i++) {
                    $extension = pathinfo($_FILES[$name]["name"][$i], PATHINFO_EXTENSION);

                    $uniqueName = $unique ? Util::getUniqueName() . "." . $extension : "";
                    move_uploaded_file($_FILES[$name]["tmp_name"][$i], $directory . ($unique ? $uniqueName : $_FILES[$name]["name"][$i]));
                    array_push($file_names, array(
                        "original" => $_FILES[$name]["name"][$i],
                        "unique" => $uniqueName
                    ));
                }
            }
        }

        return $file_names;
    }

}

?>
