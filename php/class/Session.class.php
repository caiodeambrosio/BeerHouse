<?php

class Session {
    
    function __construct(){
        
    }
    
    public static function start(){
        session_start();
    }
    
    public static function destroy(){
        session_destroy();
    }
    
    public static function is_set($name){
        return isset($_SESSION[$name]);
    }
    
    public static function un_set($name){
        unset($_SESSION[$name]);
    }
    
    /**
     * To unset, set as null
     * @param type $name
     * @param type $value
     */
    public static function set($name, $value){
        $_SESSION[$name] = $value;
    }
    
    public static function get($name){
        return isset($_SESSION[$name])? $_SESSION[$name] : null;
    }
}
