<?php

class SystemMessage {
    
    public static $SUCCESS_SETTINGS_UPDATE = "Configurações alteradas com sucesso.";
    public static $SUCCESS_ITEM_SAVED = "Item salvo com sucesso.";
    public static $SUCCESS_ITEM_UPDATED = "Item alterado com sucesso.";
    public static $ERROR_CONTACT_ADMINISTRATOR = "Um erro ocorreu, tente novamente. Se o problema persistir, contacte o administrador.";
    public static $ERROR_ALL_FIELDS_REQUIRED = "Todos os campos são requiridos.";
    
    
}