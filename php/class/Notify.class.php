<?php

class Notify{
    
    const NOTIFY_TYPE_ERROR = 1;
    const NOTIFY_TYPE_SUCCESS = 2;
    const NOTIFY_TYPE_INFO = 3;
    const NOTIFY_TYPE_WARNING = 4;
    
    private $notifications;
    
    function __construct(){
        $this->notifications['default'] = array();
    }
    
    function set($title, $message, $type, $destination = 'default'){
        
        if(!isset($this->notifications[$destination])){
            $this->notifications[$destination] = array();
        }
        
        $notify = array(
            "title" => $title,
            "message" => $message,
            "type" => $type
        );
        
        array_push($this->notifications[$destination], $notify);
    }
   
    function get($destination = 'default'){
        return $this->notifications[$destination];
    }
    
    function show($destination = 'default'){
        if(isset($this->notifications[$destination])){
            for($i=0; $i<count($this->notifications[$destination]); $i++){
                $class = "";
                switch ($this->notifications[$destination][$i]["type"]) {
                    case Notify::NOTIFY_TYPE_ERROR: {
                            $class = "alert-danger";
                        } break;
                    case Notify::NOTIFY_TYPE_SUCCESS: {
                            $class = "alert-success";
                        } break;
                    case Notify::NOTIFY_TYPE_INFO: {
                            $class = "alert-info";
                        } break;
                    case Notify::NOTIFY_TYPE_WARNING: {
                            $class = "alert-warning";
                        } break;
                }
                ?>

                <div class="alert <?= $class ?>">
                    <b><?= $this->notifications[$destination][$i]["title"] ?></b> <?= $this->notifications[$destination][$i]["message"] ?>
                </div>

                <?php
            }
        }
    }
}

/**
 * Exemplos de utilização
 */
//$notify->set("Erro", "Sua mensagem de erro aqui.", Notify::NOTIFY_TYPE_ERROR);
//$notify->set("Info", "Sua mensagem de informação aqui.", Notify::NOTIFY_TYPE_INFO);
//$notify->set("Sucesso", "Sua mensagem de sucesso aqui.", Notify::NOTIFY_TYPE_SUCCESS);

//$notify = new Notify();
//
//if($_SESSION["SP_usuario_vencido"] == 1 && strpos($_pagina, "login") != FALSE && strpos($_pagina, "esqueci_minha_senha") != FALSE){
//    $notify->set("Atenção!","Seus serviços foram <b>desativados</b>. Para ativá-los agora <a href='controle_debito.php'>clique aqui</a>.", Notify::NOTIFY_TYPE_ERROR);
//}
?>
