<?php

//session_destroy();
session_start();
require_once "./../../php/class/Session.class.php";
require_once "./../../php/class/Util.class.php";
require_once "./../../php/class/Database.class.php";
require_once "./../../php/sql/ItemLoja.php";

//Util::debug($_REQUEST);

$database = new Database();

$store = Session::is_set("store")? unserialize(Session::get("store")) : array();  // getting string

// Add item to Store Session (items separaeted by comma)
if($_REQUEST["quantity"] > 0){
    $repeated = false;
    for($i=0; $i<count($store); $i++){
        if($store[$i]["item"] == $_REQUEST["item"]){
            $repeated = true;
            $store[$i]["quantity"] = $store[$i]["quantity"] + $_REQUEST["quantity"];
        }
    }
    if(!$repeated){
        // adding item on array
        array_push($store, array(
            "item" => $_REQUEST["item"],
            "quantity" => $_REQUEST["quantity"]
        ));
    }
    
    Session::set("store", serialize($store));   // setting up Session
}
echo count($store);
