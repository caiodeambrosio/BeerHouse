<?php
//session_start();
//session_destroy();
session_start();
require_once "../class/Session.class.php";
require_once "./../../php/class/Util.class.php";
require_once "./../../php/class/Database.class.php";
require_once "./../../php/sql/ItemLoja.php";
require_once "./../../php/sql/PropriedadesLoja.php";

//Util::debug($_REQUEST);

$database = new Database();

$store = Session::is_set("store")? unserialize(Session::get("store")) : array();  // getting string

$settings = PropriedadesLoja::getSettings();

// Retrieves Content of Cart to Present on Modal
if(count($store)){
    
    $total = 0.0;
    $subtotal = 0.0;

    for($i=0; $i<count($store); $i++){
        
        $item = $store[$i]["item"];
        $quantity = $store[$i]["quantity"];

        $itemLoja = ItemLoja::getItemLoja($item);

        $subtotal = $quantity * $itemLoja["itl_valor"];
        $total = $total + $subtotal;

        ?>

        <tr>
            <td style="text-align: center;">
                <img src="./img/repo/<?= $itemLoja["itl_imagem"] ?>" style="width: 40px;" />
            </td>
            <td style="text-align: left;">
                <b><?= $itemLoja["itl_titulo"] ?></b><br>
                <?= $itemLoja["itl_valor"] ?><br>
            </td>
            <td style="text-align: center;">
                <?= $quantity ?> x 
            </td>
            <td style="text-align: right;">
                $ <?= number_format($itemLoja["itl_valor"], 2) ?>
            </td>
            <td style="text-align: right;">
                $ <?= number_format($subtotal, 2) ?>
            </td>
            <td style="text-align: right;">
                <span class="fa fa-remove cursor-pointer BtnRemoveItem" position="<?= $i ?>" style="color: red;"></span>
            </td>
        </tr>

        <?php
    }
    ?>

        <!-- Shipping Cost-->
        <?php if(!empty($settings["sts_shipping_cost"])){ ?>
        <tr>
            <td colspan="4" style="text-align: left; padding-right: 10px;">
                <b>Shipping Cost</b>
            </td>
            <td colspan="1" style="text-align: right;">
                + $ <?= $settings["sts_shipping_cost"] ?>
            </td>
            <td>
                &nbsp;
            </td>
        </tr> 
        <?php } ?>
        
        <tr>
            <td colspan="3" style="text-align: left; padding-right: 10px;">
                <b style="font-size: 16px;">Total</b>
            </td>
            <td colspan="2" style="text-align: right; font-size: 16px;">
                <b>$ <?= number_format($total + $settings["sts_shipping_cost"], 2) ?></b>
            </td>
            <td>
                &nbsp;
            </td>
        </tr> 

    <?php 
} else {
    ?>
    <tr>
            <td colspan="6" style="text-align: center; font-size: 20px;">
                <b>Cart is empty.</b>
            </td>
        </tr> 
    <?php
}
?>