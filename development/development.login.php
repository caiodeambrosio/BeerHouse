<?php
require_once "./php/inc/basic.php";

$cliente = null;

//if(Session::is_set("confirmedUser")){
//    $cliente = unserialize(Session::get("confirmedUser"));
//}
//if(Session::is_set("user") && Session::is_set("register.program")){
//    header("Location: panel.register.php");
//}

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Login": {

                $email = $_REQUEST["email"];
                $password = $_REQUEST["password"];

                if (Login::login($email, $password)) {
                    header("Location: index.php");
                } else {
                    $notify->set("Error", "Invalid email or password.", Notify::NOTIFY_TYPE_ERROR);
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
    </head>
    <body style="background-color: #F2F2F2;">

        <div class="container">
            <div class="container">

                <div class="row">

                    <div class="col-lg-3"></div>

                    <div class="col-lg-6" style="margin-top: 100px;">

                        <div class="row " style="margin-bottom: 20px; padding: 10px 0 0 0;">
                            <div class="col-lg-12" style="text-align: center; font-weight: bold;">
                                <a href="index.php"><img src="./img/repo/<?= Session::get("pub")["pub_logo"] ?>" class="img-fluid"></a>
                            </div>
                        </div>

                        <div id="login-box">
                            <form method="POST" action="gerenciar.login.php">
                                <input type="hidden" name="action" value="">
                                <legend><span class="fa fa-user "></span> Login</legend>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php $notify->show() ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtEmail">Email</label>
                                    <input type="text" class="form-control" id="TxtEmail" name="email" value="<?= isset($cliente["cli_email"]) ? $cliente["cli_email"] : "" ?>" placeholder="" tabindex="1">
                                </div>
                                <div class="form-group">
                                    <label for="txtPassword">Senha <a href="panel.forgot.password.php"><span class="fa fa-question-circle"></span></a></label>
                                    <input type="password" class="form-control" id="txtPassword" name="password" placeholder="" tabindex="2">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="submit" name="action" value="Login" class="btn btn-sm btn-block btn-primary cursor-pointer btnOnClickDisable" />
                                    </div>
                                </div>
                                <div class="row" id="login-box-additionals">
                                    <div class="col-lg-6 col-sm-12">
                                        <a href="panel.signup.php"><span class="fa fa-user-plus"></span> Ainda não tenho conta</a>
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <a href="panel.forgot.password.php"><span class="fa fa-question-circle"></span> Esqueci minha senha</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>

    </body>
</html>


<script>
    $(window).resize(function () {
        var height = $(window).height();
        var loginHeight = $("#login-box-external").height();

        var marginTop = (height > loginHeight) ? Math.round((height - loginHeight) / 3) : 0;
        $("#login-box-external").css("margin-top", marginTop + "px");
    });

    $(function () {
        $(window).trigger("resize");

        $(document).on("blur", "#TxtEmail", function () {
            var value = $(this).val();
            value = Util.toLower(value);
            $(this).val(value);
        });
    });
</script>