<?php
require_once "./php/inc/basic.php";

$admin = null;
$tipo_admin = "owner";

$modalAddFlag = false;
if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];
                $admin = Admin::getAdmin($id);

                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {
                $modalAddFlag = true;
                $admin["adm_nome"] = $_REQUEST["nome"];
                $admin["adm_tipo"] = $_REQUEST["tipo"];
                $admin["adm_email"] = $_REQUEST["email"];
                $admin["adm_password"] = Util::generatePassword();
                $admin["adm_status"] = isset($_REQUEST["status"]) ? 1 : 0;

                if (Admin::validateSaveAdmin($admin)) {
                    $id = Admin::saveAdmin($admin);
                    if (is_numeric($id)) {
                        header("location: gerenciar.owners.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                    }
                }
            } break;
        case "Update": {
                $modalAddFlag = true;

                $admin["adm_id"] = $_REQUEST["id"];
                $admin["adm_nome"] = $_REQUEST["nome"];
                $admin["adm_tipo"] = $_REQUEST["tipo"];
                $admin["adm_email"] = $_REQUEST["email"];
                $admin["adm_password"] = $_REQUEST["password"];
                $admin["adm_status"] = isset($_REQUEST["status"]) ? 1 : 0;

                if (Admin::validateUpdateAdmin($admin)) {
                    $return = Admin::updateAdmin($admin);
                    if ($return) {
                        $id = $admin["adm_id"];
                        header("location: gerenciar.owners.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
        case "Resetar Senha": {
                $modalAddFlag = true;

                $email = $_REQUEST["email"];
                $admin = Admin::getAdminByEmail($email);
                
                $admin["adm_password"] = Util::generatePassword();
                $return = Admin::updatePassword($admin);
                if ($return) {
                    $id = $admin["adm_id"];
                    header("location: gerenciar.owners.php?action=Edit&id={$id}&updated");
                } else {
                    $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                };
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>
        <?php include "./php/inc/links.php"; ?>
        <?php
        $section_title = "<span class='fa fa-users'></span> Admins (Proprietários)";
        include "./php/html/section.title.php";
        ?>

        <div class="container">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 20%;">Nome</th>
                        <th style="width: 20%;">Email</th>
                        <th style="width: 20%;">Tipo</th>
                        <th style="width: 15%;">Status</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $table = Admin::getOwners();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td><?= $table[$i]["adm_id"] ?></td>
                            <td><?= $table[$i]["adm_nome"] ?></td>
                            <td><?= $table[$i]["adm_email"] ?></td>
                            <td><?= $table[$i]["adm_tipo"] ?></td>
                            <td><?= $table[$i]["adm_status"] == 1 ? "<span class='badge badge-success'>Ativo</span>" : "<span class='badge badge-danger'>Inativo</span>" ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?= $table[$i]["adm_id"] ?>">
                                    <button type="submit" class="btn btn-primary" name="action" value="Edit">Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->

        <form method="POST" action="gerenciar.owners.php" enctype="multipart/form-data" id="FrmAdd">
            <input type="hidden" name="action" value=""> 
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $admin == null ? "Novo" : "Editar" ?> Owner</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <legend>Informações do Owner</legend>
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <?php if (is_array($admin) && isset($admin["adm_id"])) { ?>
                                <input type="hidden" name="id" value="<?= ($admin != null) ? $admin["adm_id"] : "" ?>">
                                <input type="text" class="form-control" name="id" id="txtId" value="<?= ($admin != null) ? $admin["adm_id"] : "" ?>" autocomplete="off">
                            <?php } ?>
                            <div class="form-group">
                                <label for="txtNome">Nome<span class="required">*</span></label>
                                <input type="text" class="form-control" name="nome" id="txtNome" value="<?= ($admin != null) ? $admin["adm_nome"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtEmail">Email <span class="required">*</span></label>
                                <input type="hidden" name="email" value="<?= ($admin != null) ? $admin["adm_email"] : "" ?>">
                                <input type="text" class="form-control" name="email" id="txtEmail" placeholder="" value="<?= ($admin != null) ? $admin["adm_email"] : "" ?>" <?= ($admin != null) && isset($admin["adm_id"]) ? "disabled" : "" ?> autocomplete="off">
                            </div>
                            <div class="form-group">
                                <?php include "./php/html/select.tipos_admin.php"; ?>
                            </div>
                            <div class="form-group">
                                <label for="txtPassword">Password</label>
                                <input type="hidden" name="password" value="<?= $admin != null ? $admin["adm_password"] : "" ?>">
                                <div class="input-group">
                                    <input type="password" id="txtPassword" name="password" class="form-control" value="<?= $admin != null ? $admin["adm_password"] : "" ?>"disabled/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-outline-dark" id="BtnExibirSenha" data-toggle="button" aria-pressed="false" autocomplete="off">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="button" class="btn btn-outline-danger btn-block BtnResetPassword cursor-pointer" value="Resetar Senha" <?= $admin == null ? "disabled" : "" ?>>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="status" <?= $admin != null ? ($admin["adm_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Active</span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($admin["adm_id"]) ? "Salvar" : "Update";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- MODAL CONFIRMATION -->
        <div class="modal fade modal-confirm" id="modalConfirm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmação</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">Você Confirma Esta Ação?</div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btnOnClickDisable" id="BtnModalConfirm" data-dismiss="modal" name="action" value="Confirm">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


<script>
    $(function () {
<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
            $("#txtDataNascimento").mask("99/99/9999");
            $("#txtTelefone").mask("(99) 9999-9999");
            $("#txtCelular").mask("(99) 99999-9999");
<?php } ?>
        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.owners.php?action=Add";
        });

        $(document).on("click", "#BtnExibirSenha", function () {
            if ($("#BtnExibirSenha").hasClass("active")) {
                $("#txtPassword").prop("type", "text");
            } else {
                $("#txtPassword").prop("type", "password");
            }
        });

        $(document).on("click", ".BtnResetPassword", function () {
            console.log("wlkjskldfjskld")
            var form = $("#FrmAdd");
            $(document).one("click", "#BtnModalConfirm", function () {
                $(form).find("input[name='action']").val("Resetar Senha")
                $(form).submit();
            });
            $("#modalConfirm").modal("show");
        });
    });
</script>