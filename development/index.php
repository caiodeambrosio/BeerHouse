<?php
require_once "./php/inc/basic.php";

?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>
        <?php
        include "./php/inc/links.php";
        ?>

        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-group"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.clientes.php">Clientes</a></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-shopping-bag"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.itensLoja.php">Itens da Loja</a></div>
                    </div>
                </div>
                
            </div>
        </div>

    </body>
</html>


<script>
    $(function () {

        $(".BtnMetro").click(function () {
            document.location.href = $(this).find("a").attr("href");
        });

    });
</script>