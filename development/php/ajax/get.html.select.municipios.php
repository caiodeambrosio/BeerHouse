<?php
session_start();
require_once "../../../php/class/Database.class.php";
require_once "../sql/Municipio.php";
require_once "../sql/Estado.php";

$database = new Database();

$estado_id = isset($_REQUEST["estado"])? $_REQUEST["estado"] : "";
$municipio_id = isset($_REQUEST["municipio"])? $_REQUEST["municipio"] : "";

$estado = Estado::getEstado($estado_id);
        
$table = Municipio::getMunicipiosByUF($estado["est_uf"]);

?>

<select name="municipio" id="txtMunicipio" class="form-control">
    <option></option>
    <?php
    for($cmbIterator=0; $cmbIterator<count($table); $cmbIterator++){
        ?>
        <option value="<?= $table[$cmbIterator]["mun_id"] ?>" <?= ($municipio_id == $table[$cmbIterator]["mun_id"])? "selected" : "" ?>><?= $table[$cmbIterator]["mun_nome"] ?></option>
        <?php
    }
    ?>
</select>

