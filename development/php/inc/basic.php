<?php

require_once ("./../panel/php/sql/Admin.php");


require_once ("./../php/class/Util.class.php");
require_once ("./../php/class/Notify.class.php");
require_once ("./../php/class/Database.class.php");
require_once ("./../php/class/SystemMessage.class.php");

Session::start();

$database = new Database();
$notify = new Notify();

$page = Util::getCurrentPage();

if ($page == "development.login.php") {
    if (Login::logged()) {
        header("Location: index.php");
    }
} else { // Page different than "login.php"
    if (!Login::logged()) {
        header("Location: development.login.php");
    }
}
?>