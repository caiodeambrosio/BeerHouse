
<div class="container" id="links">

    <div class="row justify-content-md-center">
        <div class="col-lg-2" style="padding: 0px 10px 10px 10px;">
            <img src="./img/logo.png" class="img-fluid">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <nav class="navbar navbar-expand navbar-dark bg-dark" style="background-color: #19475D;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a class="navbar-brand" href="#">&nbsp;</a>

                <div class="collapse navbar-collapse" id="navbarNavDropdown1">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.dashboard.php"><span class="fa fa-home"></span> Home</a>
                        </li>                     
                        <li class="nav-item active">
                           
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php"><span class="fa fa-envelope-o"></span> Inbox</a>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-institution"></span> Loja
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.clientes.php"><span class="fa fa- fa-group"></span> Clientes</a>
                                <a class="dropdown-item" href="gerenciar.itensLoja.php"><span class="fa fa-shopping-bag"></span> Itens</a>
                                <a class="dropdown-item" href="gerenciar.pedidos.php"><span class="fa fa-shopping-cart"></span> Pedidos</a>
                                <a class="dropdown-item" href="gerenciar.pagamentos.php"><span class="fa fa-credit-card"></span> Pagamentos</a>
                                <a class="dropdown-item" href="gerenciar.ofertas.php"><span class="fa fa-send"></span> Ofertas</a>
                                <a class="dropdown-item" href="gerenciar.configuracoes.php"><span class="fa fa-cog"></span> Configurações</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-globe"></span> Website
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.eventos.php"><span class="fa fa-bullhorn"></span> Eventos</a>
                            </div>
                        </li>
                         
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-globe"></span> Administração
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.administradores.php"><span class="fa fa-vcard-o"></span> Administradores</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-coffee"></span> Desenvolvimento
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.permissoes.php"><span class="fa fa-key"></span> Permissões</a>
                                <a class="dropdown-item" href="gerenciar.devtools.php"><span class="fa fa-cogs"></span> Dev. Tools</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

        </div>
    </div>

</div>

</div>
