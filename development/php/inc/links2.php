
<div class="<?= $settings["sys_container"] ?>" id="links2">

    <div class="row">
        <div class="col-lg-12">

            <nav class="navbar navbar-inverse navbar-toggleable-md" style="background-color: #50A6CF; border-radius: 0 0 6px 6px;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown2" aria-controls="navbarNavDropdown2" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <a class="navbar-brand" href="#">&nbsp;</a>
                
                <div class="collapse navbar-collapse" id="navbarNavDropdown2">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.dashboard.php"><span class="fa fa-th-large"></span> Dashboard</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.notifications.php"><span class="fa fa-bell"></span> Notifications</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.inbox.php"><span class="fa fa-envelope"></span> Inbox</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.registrations.php">Registrations</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.offers.php">Offers</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.payments.php">Payments</a>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-globe"></span> Store
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.storeOrders.php">Orders</a>
                                <a class="dropdown-item" href="gerenciar.storeItems.php">Items</a>
                                <a class="dropdown-item" href="gerenciar.storeSettings.php">Settings</a>
                                
                            </div>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-globe"></span> Website
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.banners.php">Banners</a>
                                <a class="dropdown-item" href="gerenciar.about.php">About</a>
                                <a class="dropdown-item" href="gerenciar.news.php">News</a>
                                <a class="dropdown-item" href="gerenciar.faq.php">FAQ</a>
                                
                            </div>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="fa fa-globe"></span> Other
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="gerenciar.regions.php">Regions</a>
                                <a class="dropdown-item" href="gerenciar.locations.php">Locations</a>
                                <a class="dropdown-item" href="gerenciar.coaches.php">Coaches & Trainers</a>
                                <a class="dropdown-item" href="gerenciar.teams.php">Teams</a>
                                <a class="dropdown-item" href="gerenciar.users.php">Users</a>
                                <a class="dropdown-item" href="gerenciar.players.php">Players</a>
                                <a class="dropdown-item" href="gerenciar.programs.php">Programs & Clinics</a>
                                <a class="dropdown-item" href="gerenciar.waivers.php">Waivers</a>
                                <a class="dropdown-item" href="gerenciar.history.php">Login History</a>
                                <a class="dropdown-item" href="gerenciar.systemSettings.php">System Settings</a>
                                
                            </div>
                        </li>
                        
<!--                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.players.php">Players</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.store.php">Store</a>
                        </li>
                        
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.notifications.php">Notifications</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.regions.php">Regions</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.locations.php">Locations</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.coaches.php">Coaches & Trainers</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.teams.php">Teams</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.programs.php">Programs</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.history.php">Login History</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.registrations.php">Registrations</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.offers.php">Offers</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.payments.php">Payments</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.store.php">Store</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.news.php">News</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="gerenciar.banners.php">Banners</a>
                        </li>-->
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="http://www.abbeyvillasoccer.com" target="_blank"><span class="fa fa-external-link"></span> Open Website</a>
                        </li>
                    </ul>
                </div>
            </nav>

        </div>
    </div>

</div>

