<?php
require_once "./php/inc/basic.php";

$VIEW = isset($_REQUEST["list"]) ? "list" : "grid";

$item = Session::get("pub");
if (isset($_REQUEST["action"])) {
//    switch($_REQUEST["action"]){
//        case "Add to Cart":{
//            
//        } break;
//    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
        <link type="text/css" rel="stylesheet" href="./css/store.css" />
        <script type="text/javascript" src="js/loja.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><?= $item["pub_titulo"] ?></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="btn btn-sm btn-success" title="" style="margin-right: 10px;"><span class="fa fa-shopping-cart"></span>&nbsp; Carrinho &nbsp;&nbsp;<span class="badge badge-pill badge-light cart-items-number">0</span></a>
<!--                            <a href="javascript:void(0);" class="btnOpenCart btn btn-outline-success" title=""><span class="fa fa-shopping-cart"></span>&nbsp; Open Cart &nbsp;&nbsp;<span class="badge badge-pill badge-success cart-items-number">0</span></a>-->
                        </li>
                        <li class="nav-item">
                            <a href="checkout.php" class="btn btn-sm btn-primary" title=""><span class="fa fa-credit-card"></span>&nbsp; Finalizar Compra</a>
                            <!--<a href="checkout.php" class="btnProceedToCheckout btn btn-outline-primary" title=""><span class="fa fa-credit-card"></span>&nbsp; Proceed to Checkout</a>-->
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <header class="masthead" style="background-image: url('./img/repo/<?= $item["pub_banner"] ?>')">
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in"><?= $item["pub_descricao"] ?></div>
                    <div class="intro-heading text-uppercase"><?= $item["pub_titulo"] ?></div>
                    <?php if ($item["pub_exibir_botao"] == 1) { ?>
                        <a class="btn btn-primary2 btn-xl text-uppercase js-scroll-trigger" href="<?= $item["pub_link_botao"] ?>"><?= $item["pub_texto_botao"] ?></a>
                    <?php } ?>
                </div>
            </div>
        </header>
        <section class="bg-light" id="loja">
            <div class="container">
                <!-- Options Bar -->
                <div class="row" style="margin-bottom: 40px;">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-2">
                                <a class="btn btn-outline-primary" href="loja.php?grid" title="See as Grid" <?= ($VIEW == "grid" ? "disabled" : "") ?>><span class="fa fa-th-large"></span></a>
                                <a class="btn btn-outline-primary" href="loja.php?list" title="See as List" <?= ($VIEW == "list" ? "disabled" : "") ?>><span class="fa fa-th-list"></span></a>
                            </div>
                            <div class="col-lg-4">

                            </div>
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-3">

                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <div class="row">

                            <?php
                            $table = ItemLoja::getItensLoja("itl_active = 1");
                            for ($i = 0; $i < count($table); $i++) {
                                $item = $table[$i]["itl_id"];
                                if ($VIEW == "grid") {
                                    ?>

                                    <div class="col-lg-4">

                                        <form method="POST" action="loja.php">
                                            <div class="store-item" item="<?= $item ?>">
                                                <div class="store-item-image">
                                                    <img src="./img/repo/<?= $table[$i]["itl_imagem"] ?>" class="img-fluid">
                                                </div>
                                                <div class="store-item-name">
                                                    <?= $table[$i]["itl_titulo"] ?>
                                                </div>
                                                <div class="store-item-price" item="<?= $item ?>">

                                                </div>
                                                <div class="store-item-quantity">
                                                    <div class="input-group">
                                                        <span class="input-group-addon btn-outline-primary cursor-pointer btnPlus" refid="#quantity_<?= $i ?>"><span class="fa fa-plus"></span></span>
                                                        <input type="text" name="quantity" value="0" class="form-control txtQuantity" maxlength="2" id="quantity_<?= $i ?>" item="<?= $item ?>"/>
                                                        <span class="input-group-addon btn-outline-primary cursor-pointer btnMinus" refid="#quantity_<?= $i ?>"><span class="fa fa-minus"></span></span>
                                                    </div>
                                                </div>
                                                <div class="store-item-button">
                                                    <input type="button" value="Add to Cart" item="<?= $item ?>" class="btnAddToCart btn btn-sm btn-block btn-success cursor-pointer" />
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                    <?php
                                } else {
                                    ?>

                                    <div class="col-lg-12">

                                        <form method="POST" action="loja.php">
                                            <div class="row store-item" item="<?= $item ?>">

                                                <div class="col-lg-6">
                                                    <div class="store-item-image">
                                                        <img src="./panel/img/repo/<?= $table[$i]["itl_imagem"] ?>" class="img-fluid">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="row store-item-variant-quantity">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="store-item-name">
                                                                <?= $table[$i]["itl_titulo"] ?>
                                                            </div>
                                                            <div class="store-item-price" item="<?= $item ?>">
                                                                <?= $table[$i]["itl_variants"][0]["stv_instock"] == 1 ? "In Stock" : "" ?> - $ <?= $table[$i]["itl_variants"][0]["stv_price"] ?>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <div class="store-item-quantity">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon btn-outline-primary cursor-pointer btnPlus" refid="#quantity_<?= $i ?>"><span class="fa fa-plus"></span></span>
                                                                    <input type="text" name="quantity" value="0" class="form-control txtQuantity" maxlength="2" id="quantity_<?= $i ?>" />
                                                                    <span class="input-group-addon btn-outline-primary cursor-pointer btnMinus" refid="#quantity_<?= $i ?>"><span class="fa fa-minus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>   
                                                    </div>  
                                                    <div class="store-item-button">
                                                        <input type="button" value="Add to Cart" item="<?= $item ?>" class="btnAddToCart btn btn-sm btn-block btn-success cursor-pointer" />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                    <?php
                                }
                            }
                            ?>

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="container">

                <!--                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="box-page-title">
                                            Store
                                        </div>
                                    </div>
                                </div>-->




            </div>
        </div>

        <?php // include "./php/inc/footer.php"  ?>

        <div class="modal fade" id="modal-store">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><span class="fa fa-shopping-cart"></span> Your Items</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-success btn-block cursor-pointer" data-dismiss="modal" style="margin-bottom: 10px;">Continue Shopping</button>
                            </div>
                            <div class="col-lg-6">
                                <a href="checkout.php" class="btn btn btn-block btn-primary">Proceed to Checkout</a>
                            </div>
                        </div>

                        <table class="table" style="width: 100%; font-size: 13px; padding-right: 15px;">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">&nbsp;</th>
                                    <th style="text-align: left;">Item</th>
                                    <th style="text-align: center;">Quantity</th>
                                    <th style="text-align: right;">Unit Price</th>
                                    <th style="text-align: right;">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody id="modal-store-table-body">

                            </tbody>
                        </table>

                        <div class="row">
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-success btn-block cursor-pointer" data-dismiss="modal" style="margin-bottom: 10px;">Continue Shopping</button>
                            </div>
                            <div class="col-lg-6">
                                <a href="checkout.php" class="btn btn btn-block btn-primary">Proceed to Checkout</a>
                            </div>
                        </div>

                    </div>
                    <!--                    <div class="modal-footer">
                                            <a href="checkout.php" class="btn btn-sm btn-primary">Proceed to Checkout</a>
                                        </div>-->
                </div>
            </div>
        </div>

    </body>
    <script type="text/javascript" src="js/agency.js"></script>
</html>
