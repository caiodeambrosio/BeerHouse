<?php
require_once "./php/inc/basic.php";
$cliente = null;

//if(Session::is_set("confirmedUser")){
//    $cliente = unserialize(Session::get("confirmedUser"));
//}
//if(Session::is_set("user") && Session::is_set("register.program")){
//    header("Location: panel.register.php");
//}

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Login": {

                $email = $_REQUEST["email"];
                $password = $_REQUEST["password"];

                if (Login::login($email, $password)) {
                    header("Location: index.php");
                } else {
                    $notify->set("Erro", "Email ou Senha não conferem.", Notify::NOTIFY_TYPE_ERROR);
                }
            } break;
    }
}
?>

<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
        <link type="text/css" rel="stylesheet" href="css/login.css" />
        <!--<script type="text/javascript" src="./js/login.js"></script>-->
    </head>
    <body style="background-color: #F2F2F2;">

        <div class="container-fluid">
            <div class="container">

                <div class="row">

                    <div class="col-lg-3">

                    </div>

                    <div class="col-lg-6">

                        <div id="login-box-external">

                            <div class="row" style="margin-bottom: 20px; padding: 80px 0 0 0;">
                                <div class="col-lg-12" style="text-align: center; font-weight: bold;">
                                    <?php 
                                    $logo = Session::get("pub") != null ? "./../img/repo/" . Session::get("pub")["pub_logo"] : "./../img/logo.png";  
                                    ?>
                                    
                                    <img src="<?= $logo ?>" class="img-fluid">
                                </div>
                            </div>

                            <div id="login-box">
                                <form method="POST" action="gerenciar.login.php">
                                    <input type="hidden" name="action" value="">
                                    <div id="login-box-title">
                                        <span class="fa fa-user-secret"></span> Login
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php $notify->show() ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtEmail">Email</label>
                                        <input type="text" class="form-control" id="txtEmail" name="email" placeholder="" tabindex="1">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtPassword">Senha</label>
                                        <input type="password" class="form-control" id="txtPassword" name="password" placeholder="" tabindex="2">
                                    </div>
                                    <!--                                    <div class="form-group row">
                                                                            <label class="col-sm-2">&nbsp;</label>
                                                                            <div class="col-sm-10">
                                                                                <div class="form-check">
                                                                                    <label class="form-check-label">
                                                                                        <input class="form-check-input" type="checkbox"> Remember me on this computer
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" name="action" value="Login" class="btn btn-sm btn-block btn-primary cursor-pointer btnOnClickDisable" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                    <div class="col-lg-3">

                    </div>

                </div>

            </div>
        </div>

    </body>
</html>


<script>
    $(window).resize(function () {
        var height = $(window).height();
        var loginHeight = $("#login-box-external").height();

        var marginTop = (height > loginHeight) ? Math.round((height - loginHeight) / 3) : 0;
        $("#login-box-external").css("margin-top", marginTop + "px");
    });

    $(function () {
        $(window).trigger("resize");

        $(document).on("blur", "#TxtEmail", function () {
            var value = $(this).val();
            value = Util.toLower(value);
            $(this).val(value);
        });
    });
</script>