<?php
require_once "./php/inc/basic.php";

$cliente = null;

$modalAddFlag = false;
if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];

                $cliente = Cliente::getCliente($id);
                $cliente["cli_data_nascimento"] = Util::getDateToShow($cliente["cli_data_nascimento"]);

                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {
                $modalAddFlag = true;
                $cliente["cli_nome"] = $_REQUEST["nome"];
                $cliente["cli_sobrenome"] = $_REQUEST["sobrenome"];
                $cliente["cli_telefone"] = $_REQUEST["telefone"];
                $cliente["cli_celular"] = $_REQUEST["celular"];
                $cliente["cli_email"] = $_REQUEST["email"];
                $cliente["cli_sexo"] = $_REQUEST["sexo"];
                $cliente["cli_data_nascimento"] = Util::getDateToDatabase($_REQUEST["data_nascimento"]);
                $cliente["cli_password"] = Util::generatePassword();
                $cliente["cli_rua"] = $_REQUEST["rua"];
                $cliente["cli_numero"] = $_REQUEST["numero"];
                $cliente["cli_bairro"] = $_REQUEST["bairro"];
                $cliente["cli_ponto_referencia"] = $_REQUEST["ponto_referencia"];
                $cliente["cli_id_estado"] = $_REQUEST["estado"];
                $cliente["cli_id_municipio"] = $_REQUEST["municipio"];
                $cliente["cli_status"] = 0;

                if (Cliente::validarSaveCliente($cliente)) {
                    if (Cliente::checkIfEmailExists($cliente["cli_email"])) {
                        // Usuario ja existe
                        $clienteTemp = Cliente::getClienteByEmail($cliente["cli_email"]);
                        if ($clienteTemp != null && $clienteTemp["cli_status"] == 0) {
                            // Already a user but did not confirm the email (sends email of confirmation)
                        }
                    } else {
                        // Not user yet.
                        $id = Cliente::salvarCliente($cliente);
                        if (is_numeric($id)) {
                            header("location: gerenciar.clientes.php?action=Edit&id={$id}&saved");
                        } else {
                            $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                        }
                    }
                }
            } break;
        case "Update": {
                $modalAddFlag = true;
                $cliente["cli_id"] = $_REQUEST["id"];
                $cliente["cli_nome"] = $_REQUEST["nome"];
                $cliente["cli_sobrenome"] = $_REQUEST["sobrenome"];
                $cliente["cli_telefone"] = $_REQUEST["telefone"];
                $cliente["cli_celular"] = $_REQUEST["celular"];
                $cliente["cli_email"] = $_REQUEST["email"];
                $cliente["cli_sexo"] = $_REQUEST["sexo"];
                $cliente["cli_data_nascimento"] = Util::getDateToDatabase($_REQUEST["data_nascimento"]);
                $cliente["cli_password"] = $_REQUEST["password"];
                $cliente["cli_rua"] = $_REQUEST["rua"];
                $cliente["cli_numero"] = $_REQUEST["numero"];
                $cliente["cli_bairro"] = $_REQUEST["bairro"];
                $cliente["cli_ponto_referencia"] = $_REQUEST["ponto_referencia"];
                $cliente["cli_id_estado"] = $_REQUEST["estado"];
                $cliente["cli_id_municipio"] = $_REQUEST["municipio"];
                $cliente["cli_status"] = 0;


                if (Cliente::validarUpdateCliente($cliente)) {
                    $return = Cliente::updateCliente($cliente);
                    if ($return) {
                        $id = $cliente["cli_id"];
                        header("location: gerenciar.clientes.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;

        case "Resetar Senha": {
                $modalAddFlag = true;

                $email = $_REQUEST["email"];

                if (Cliente::validarResetarSenha($email)) {
                    $cliente = Cliente::getClienteByEmail($email);
                    $cliente["cli_password"] = Util::generatePassword();
                    $return = Cliente::updateCliente($cliente);
                    if ($return) {
                        $id = $cliente["cli_id"];
                        header("location: gerenciar.clientes.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
    </head>
    <body>

        <div class="container-fluid">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-users'></span> Clientes";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 20%;">Name</th>
                        <th style="width: 20%;">Email</th>
                        <th style="width: 20%;">Telefone</th>
                        <th style="width: 15%;">Status</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $table = Cliente::getClientes();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td><?= $table[$i]["cli_id"] ?></td>
                            <td><?= $table[$i]["cli_nome"] . " " . $table[$i]["cli_sobrenome"] ?></td>
                            <td><?= $table[$i]["cli_email"] ?></td>
                            <td><?= $table[$i]["cli_telefone"] . "<br> " . $table[$i]["cli_celular"] ?></td>
                            <td><?= $table[$i]["cli_status"] == 1 ? "<span class='badge badge-success'>Ativo</span>" : "<span class='badge badge-danger'>Inativo</span>" ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?= $table[$i]["cli_id"] ?>">
                                    <button type="submit" class="btn btn-primary" name="action" value="Edit">Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->

        <form method="POST" action="gerenciar.clientes.php" enctype="multipart/form-data"  id="FrmAdd">
            <input type="hidden" name="action" value=""> 
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $cliente == null ? "Novo" : "Editar" ?> Cliente</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <?php if (is_array($cliente) && isset($cliente["cli_id"])) { ?>
                                <input type="hidden" name="id" value="<?= ($cliente != null) ? $cliente["cli_id"] : "" ?>">
                            <?php } ?>
                            <legend>Informações do Cliente</legend>
                            <div class="form-group">
                                <label for="txtNome">Nome<span class="required">*</span></label>
                                <input type="text" class="form-control" name="nome" id="txtNome" value="<?= ($cliente != null) ? $cliente["cli_nome"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtSobrenome">Sobrenome <span class="required">*</span></label>
                                <input type="text" class="form-control" name="sobrenome" id="txtSobrenome" placeholder="" value="<?= ($cliente != null) ? $cliente["cli_sobrenome"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtTelefone">Celular</label>
                                <input type="text" class="form-control" name="celular" id="txtCelular" placeholder="" value="<?= ($cliente != null) ? $cliente["cli_celular"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtTelefone">Telefone</label>
                                <input type="text" class="form-control" name="telefone" id="txtTelefone" placeholder="" value="<?= ($cliente != null) ? $cliente["cli_telefone"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtEmail">Email <span class="required">*</span></label>
                                <input type="text" class="form-control" name="email" id="txtEmail" placeholder="" value="<?= ($cliente != null) ? $cliente["cli_email"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtGender">Sexo <span class="required">*</span></label>
                                <select name="sexo" class="form-control">
                                    <option>Escolha</option>
                                    <option value="Masculino" <?= ($cliente["cli_sexo"] == "Masculino") ? "selected" : "" ?>>Masculino</option>
                                    <option value="Feminino" <?= ($cliente["cli_sexo"] == "Feminino") ? "selected" : "" ?>>Feminino</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtDataNascimento">Data de Nascimento <span class="required">*</span></label>
                                <input type='text' class='form-control fldDate' name='data_nascimento' id="txtDataNascimento" value="<?= $cliente != null ? $cliente["cli_data_nascimento"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtPassword">Password</label>
                                <input type="hidden" name="password" value="<?= $cliente != null ? $cliente["cli_password"] : "" ?>">
                                <div class="input-group">
                                    <input type="password" id="txtPassword" name="password" class="form-control" value="<?= $cliente != null ? $cliente["cli_password"] : "" ?>"disabled/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-outline-dark" id="BtnExibirSenha" data-toggle="button" aria-pressed="false" autocomplete="off">
                                            <span class="fa fa-eye"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="button" class="btn btn-outline-danger btn-block BtnResetPassword cursor-pointer" value="Resetar Senha" <?= $cliente == null ? "disabled" : "" ?>>
                            </div>
                            <br>
                            <legend>Endereço</legend>
                            <div class="form-group">
                                <label for="txtRua">Rua <span class="required">*</span></label>
                                <input type="text" class="form-control" name="rua" id="txtRua" value="<?= ($cliente != null) ? $cliente["cli_rua"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtNumero">Numero <span class="required">*</span></label>
                                <input type="text" class="form-control" name="numero" id="txtNumero" value="<?= ($cliente != null) ? $cliente["cli_numero"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtBairro">Bairro <span class="required">*</span></label>
                                <input type="text" class="form-control" name="bairro" id="txtBairro" value="<?= ($cliente != null) ? $cliente["cli_bairro"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtEstado">Estado <span class="required">*</span></label>
                                <?php
                                $estado_id = ($cliente != null ? $cliente["cli_id_estado"] : "");
                                include "./php/html/select.estados.php";
                                ?>
                            </div>
                            <div class="form-group">
                                <label for="txtCidade">Cidade <span class="required">*</span></label>
                                <div id="ctnMunicipio">
                                    <?php
                                    $municipio_id = (isset($cliente["cli_id_municipio"]) && $cliente != null ? $cliente["cli_id_municipio"] : "");
                                    if (is_numeric($estado_id) || is_numeric($municipio_id)) {
                                        include "./php/html/select.municipios.php";
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtPontoReferencia">Ponto de Referencia <span class="required">*</span></label>
                                <input type="text" class="form-control" name="ponto_referencia" id="txtPontoReferencia" placeholder="" value="<?= ($cliente != null) ? $cliente["cli_ponto_referencia"] : "" ?>">
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="status" <?= $cliente != null ? ($cliente["cli_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Active</span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($cliente["cli_id"]) ? "Salvar" : "Update";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- MODAL CONFIRMATION -->
        <div class="modal fade modal-confirm" id="modalConfirm">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Confirmação</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">Você Confirma Esta Ação?</div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btnOnClickDisable" id="BtnModalConfirm" data-dismiss="modal" name="action" value="Confirm">Confirmar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


<script>
    $(function () {
<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
            $("#txtDataNascimento").mask("99/99/9999");
            $("#txtTelefone").mask("(99) 9999-9999");
            $("#txtCelular").mask("(99) 99999-9999");
<?php } ?>
        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.clientes.php?action=Add";
        });

        $(document).on("click", "#BtnExibirSenha", function () {
            if ($("#BtnExibirSenha").hasClass("active")) {
                $("#txtPassword").prop("type", "text");
            } else {
                $("#txtPassword").prop("type", "password");
            }
        });

        $(document).on("click", ".BtnResetPassword", function () {
            var form = $("#FrmAdd");
            $(document).one("click", "#BtnModalConfirm", function () {
                $(form).find("input[name='action']").val("Resetar Senha")
                $(form).submit();
            });
            $("#modalConfirm").modal("show");
        });

        /* Event on txtEstado */
        $(document).on("change", "#txtEstado", function () {
            var estado = $(this).val();
            $("#ctnMunicipio").html("<img src='./img/loading.gif' class='loading-gif'>");
            $.post("./php/ajax/get.html.select.municipios.php", {
                estado: estado
            }, function (r) {
                $("#ctnMunicipio").html(r);
            });
        });
    });
</script>