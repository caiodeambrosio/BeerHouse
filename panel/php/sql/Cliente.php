<?php

class Cliente {

    function __construct() {
        
    }

    public static function validarSaveCliente($cliente) {
        global $notify;

        if (empty($cliente["cli_nome"])) {
            $notify->set("Erro: ", "Campo <b>Nome</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_sobrenome"])) {
            $notify->set("Erro: ", "Campo <b>Sobrenome</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_celular"]) && empty($cliente["cli_telefone"])) {
            $notify->set("Erro: ", "É necessário informar <b>Telefone ou Celular</b>", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_password"])) {
            $notify->set("Erro: ", "Campo <b>Password</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_sexo"])) {
            $notify->set("Erro: ", "Campo <b>Sexo</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_data_nascimento"])) {
            $notify->set("Erro: ", "Campo <b>Data de Nascimento</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_id_estado"])) {
            $notify->set("Erro: ", "Campo <b>Estado</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_id_municipio"])) {
            $notify->set("Erro: ", "Campo <b>Municipio</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_rua"])) {
            $notify->set("Erro: ", "Campo <b>Rua</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_numero"])) {
            $notify->set("Erro: ", "Campo <b>Numero</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_bairro"])) {
            $notify->set("Erro: ", "Campo <b>Bairro</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (Cliente::checkIfEmailExists($cliente["cli_email"])) {
            $notify->set("Erro: ", "Já existe um Usuário Cadastrado com este Email", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function validarUpdateCliente($cliente) {
        global $notify;

        if (empty($cliente["cli_nome"])) {
            $notify->set("Erro: ", "Campo <b>Nome</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_sobrenome"])) {
            $notify->set("Erro: ", "Campo <b>Sobrenome</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_celular"]) && empty($cliente["cli_telefone"])) {
            $notify->set("Erro: ", "É necessário informar <b>Telefone ou Celular</b>", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_password"])) {
            $notify->set("Erro: ", "Campo <b>Password</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_sexo"])) {
            $notify->set("Erro: ", "Campo <b>Sexo</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_data_nascimento"])) {
            $notify->set("Erro: ", "Campo <b>Data de Nascimento</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_id_estado"])) {
            $notify->set("Erro: ", "Campo <b>Estado</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_id_municipio"])) {
            $notify->set("Erro: ", "Campo <b>Municipio</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_rua"])) {
            $notify->set("Erro: ", "Campo <b>Rua</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_numero"])) {
            $notify->set("Erro: ", "Campo <b>Numero</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($cliente["cli_bairro"])) {
            $notify->set("Erro: ", "Campo <b>Bairro</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function validarResetarSenha($email) {
        global $notify;
        if (empty($email)) {
            $notify->set("Erro: ", "Campo <b>Email Address</b> é obrigatório.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function salvarCliente($cliente) {
        global $database;

        $query = "INSERT tbl_cli_cliente";
        $query .= "(";
        $query .= " cli_id_pub, ";
        $query .= " cli_nome, ";
        $query .= " cli_sobrenome, ";
        $query .= " cli_celular, ";
        $query .= " cli_telefone, ";
        $query .= " cli_email, ";
        $query .= " cli_sexo, ";
        $query .= " cli_data_nascimento, ";
        $query .= " cli_password, ";
        $query .= " cli_rua, ";
        $query .= " cli_numero, ";
        $query .= " cli_bairro, ";
        $query .= " cli_ponto_referencia, ";
        $query .= " cli_id_estado, ";
        $query .= " cli_id_municipio, ";
        $query .= " cli_status ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '". Session::get("pub")["pub_id"] ."', ";
        $query .= " '{$cliente["cli_nome"]}', ";
        $query .= " '{$cliente["cli_sobrenome"]}', ";
        $query .= " '{$cliente["cli_celular"]}', ";
        $query .= " '{$cliente["cli_telefone"]}', ";
        $query .= " '{$cliente["cli_email"]}', ";
        $query .= " '{$cliente["cli_sexo"]}', ";
        $query .= " '{$cliente["cli_data_nascimento"]}', ";
        $query .= " '{$cliente["cli_password"]}', ";
        $query .= " '{$cliente["cli_rua"]}', ";
        $query .= " '{$cliente["cli_numero"]}', ";
        $query .= " '{$cliente["cli_bairro"]}', ";
        $query .= " '{$cliente["cli_ponto_referencia"]}', ";
        $query .= " '{$cliente["cli_id_estado"]}', ";
        $query .= " '{$cliente["cli_id_municipio"]}', ";
        $query .= " '{$cliente["cli_status"]}' ";
        $query .= ")";

        $database->query($query);
        return $database->insertId();
    }

    public static function updateCliente($cliente) {
        global $database;

        $query = "UPDATE tbl_cli_cliente ";
        $query .= "SET ";
        $query .= "cli_nome = '{$cliente["cli_nome"]}', ";
        $query .= "cli_sobrenome = '{$cliente["cli_sobrenome"]}', ";
        $query .= "cli_email = '{$cliente["cli_email"]}', ";
        $query .= "cli_sexo = '{$cliente["cli_sexo"]}', ";
        $query .= "cli_data_nascimento = '{$cliente["cli_data_nascimento"]}', ";
        $query .= "cli_password = '{$cliente["cli_password"]}', ";
        $query .= "cli_rua = '{$cliente["cli_rua"]}', ";
        $query .= "cli_numero = '{$cliente["cli_numero"]}', ";
        $query .= "cli_bairro = '{$cliente["cli_bairro"]}', ";
        $query .= "cli_ponto_referencia = '{$cliente["cli_ponto_referencia"]}', ";
        $query .= "cli_status = '{$cliente["cli_status"]}', ";
        $query .= "cli_celular = '{$cliente["cli_celular"]}', ";
        $query .= "cli_telefone = '{$cliente["cli_telefone"]}', ";
        $query .= "WHERE ";
        $query .= "cli_id = '{$cliente["cli_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function getCliente($id) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_cli_cliente ";
        $query .= "WHERE cli_id = {$id}";
        return $database->getOne($query);
    }

    public static function getClientes() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_cli_cliente ";
        $query .= "WHERE cli_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        $query .= "ORDER BY cli_nome";

        return $database->selectAll($query);
    }

    public static function getClienteByEmail($email) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_cli_cliente ";
        $query .= "WHERE cli_email LIKE '{$email}' ";
        return $database->getOne($query);
    }

    public static function getClienteByLogin($email, $password) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_cli_cliente ";
        $query .= " WHERE cli_email LIKE '{$email}' ";
        $query .= "   AND cli_password LIKE '{$password}' ";
        $query .= "   AND cli_status = 1 ";

        return $database->getOne($query);
    }

    public static function checkIfEmailExists($email) {
        global $database;
        $query = "SELECT * ";
        $query .= "  FROM tbl_cli_cliente ";
        $query .= " WHERE cli_email LIKE '{$email}' ";

        return (!empty($database->getOne($query))) ? true : false;
    }

    public static function updatePassword($cliente) {

        global $database;

        $query = "UPDATE tbl_cli_cliente ";
        $query .= "SET ";
        $query .= "cli_password = '{$cliente["cli_new_password"]}', ";
        $query .= "WHERE ";
        $query .= "cli_id = '{$cliente["cli_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

}
