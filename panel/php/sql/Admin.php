<?php

class Admin {

    function __construct() {
        
    }

    public static function validateSaveAdmin($admin) {
        global $notify;

        if (empty($admin["adm_nome"])) {
            $notify->set("Erro: ", "Campo <b>Nome</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_tipo"])) {
            $notify->set("Erro: ", "Campo <b>Tipo</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_password"])) {
            $notify->set("Erro: ", "Campo <b>Password</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (Admins::checkIfEmailExists($admin["adm_email"])) {
            $notify->set("Erro: ", "Já existe um Usuário Cadastrado com este Email", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function validateUpdateAdmin($admin) {
        global $notify;

        if (empty($admin["adm_nome"])) {
            $notify->set("Erro: ", "Campo <b>Nome</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_tipo"])) {
            $notify->set("Erro: ", "Campo <b>Tipo</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_email"])) {
            $notify->set("Erro: ", "Campo <b>Email</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else if (empty($admin["adm_password"])) {
            $notify->set("Erro: ", "Campo <b>Password</b> é obrigatorio.", Notify::NOTIFY_TYPE_ERROR, "modalAdd");
        } else {
            return true;
        }
        return false;
    }

    public static function getAdminByLogin($email, $password) {
        global $database;

        $query = "SELECT * ";
        $query .= "  FROM tbl_adm_admin ";
        $query .= " WHERE adm_email LIKE '{$email}' ";
        $query .= "   AND adm_password LIKE '{$password}' ";
        return $database->getOne($query);
    }

    public static function getAdmin($id) {
        global $database;

        $query = "SELECT * ";
        $query .= "  FROM tbl_adm_admin ";
        $query .= " WHERE adm_id LIKE '{$id}' ";

        return $database->getOne($query);
    }

    public static function getAdminByEmail($email) {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_adm_admin ";
        $query .= "WHERE adm_email LIKE '{$email}' ";

        return $database->getOne($query);
    }

    public static function getAdmins($where = "") {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_adm_admin ";
        $query .= "WHERE adm_id != 1 ";
        $query .= "AND adm_id_pub = '" . Session::get("pub")["pub_id"] . "' ";
        if (!empty($where)) {
            $query .= "AND {$where} ";
        }
        $query .= "ORDER BY adm_name ASC ";

        return $database->selectAll($query);
    }

    public static function getOwners() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_adm_admin ";
        $query .= "WHERE adm_tipo LIKE 'owner' ";
        $query .= "ORDER BY adm_nome ASC ";

        return $database->selectAll($query);
    }

    public static function saveAdmin($admin) {

        global $database;

        $query = "INSERT INTO tbl_adm_admin ";
        $query .= "(";
        $query .= " adm_id_pub, ";
        $query .= " adm_nome, ";
        $query .= " adm_tipo, ";
        $query .= " adm_email, ";
        $query .= " adm_password, ";
        $query .= " adm_status ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '". Session::get("pub")["pub_id"] ."', ";
        $query .= " '{$admin["adm_nome"]}', ";
        $query .= " '{$admin["adm_tipo"]}', ";
        $query .= " '{$admin["adm_email"]}', ";
        $query .= " '{$admin["adm_password"]}', ";
        $query .= " '{$admin["adm_status"]}' ";
        echo $query .= ")";



        if ($database->query($query)) {
            return $database->insertId();
        }
        return false;
    }

    public static function updateAdmin($admin) {

        global $database;

        $query = "UPDATE tbl_adm_admin ";
        $query .= "SET ";
        $query .= "adm_nome = '{$admin["adm_nome"]}', ";
        $query .= "adm_tipo = '{$admin["adm_tipo"]}', ";
        $query .= "adm_email = '{$admin["adm_email"]}', ";
        $query .= "adm_status = '{$admin["adm_status"]}' ";
        $query .= "WHERE ";
        echo$query .= "adm_id = '{$admin["adm_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function updatePassword($admin) {

        global $database;

        $query = "UPDATE tbl_adm_admin ";
        $query .= "SET ";
        $query .= "adm_password = '{$admin["adm_password"]}' ";
        $query .= "WHERE ";
        echo $query .= "adm_id = '{$admin["adm_id"]}';";

        return $database->update($query) == 1 ? true : false;
    }

    public static function checkIfEmailExists($email) {
        global $database;
        $query = "SELECT * ";
        $query .= "  FROM tbl_adm_admin ";
        $query .= " WHERE adm_email LIKE '{$email}' ";

        return (!empty($database->getOne($query))) ? true : false;
    }

}
