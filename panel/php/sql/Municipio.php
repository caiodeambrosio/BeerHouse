<?php

class Municipio {

    function __construct() {
        
    }

    public static function getMunicipio($id) {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_mun_municipio ";
        $query .= "WHERE ";
        $query .= "mun_id = '{$id}';";
        return $database->getOne($query);
    }

    public static function getMunicipios() {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_mun_municipio ";
        $query .= "ORDER BY mun_nome ASC ";
        return $database->selectAll($query);
    }

    public static function getMunicipiosByUF($uf) {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_mun_municipio ";
        $query .= "WHERE ";
        $query .= "mun_uf LIKE '{$uf}';";
        
        return $database->selectAll($query);
    }
}
