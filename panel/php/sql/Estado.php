<?php

class Estado {

    function __construct() {
        
    }

    public static function getEstado($id) {
        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_est_estado ";
        $query .= "WHERE ";
        $query .= "est_id = '{$id}';";
        return $database->getOne($query);
    }

    public static function getEstados() {

        global $database;
        $query = "SELECT * ";
        $query .= "FROM tbl_est_estado ";
        $query .= "ORDER BY est_nome ASC ";
        return $database->selectAll($query);
    }

}
