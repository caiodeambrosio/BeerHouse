<?php

class PropriedadesLoja {

    function __construct() {
        
    }

    public static function existPropriedades() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_propriedades_loja ";
        $query .= "WHERE prl_id_pub = " . Session::get("pub")["pub_id"];

        return empty($database->selectAll($query)) ? false : true;
    }

    public static function getPropriedade() {
        global $database;

        $query = "SELECT * ";
        $query .= "FROM tbl_propriedades_loja ";
        $query .= "WHERE prl_id_pub = " . Session::get("pub")["pub_id"];
        return $database->getOne($query);
    }

    public static function getPropriedades() {
        global $database;
        $propriedades = array();

        $query = "SELECT * ";
        $query .= "FROM tbl_propriedades_loja ";
        $query .= "WHERE prl_id_pub = " . Session::get("pub")["pub_id"];

        return $database->selectAll($query);
    }

    public static function savePropriedades($propriedades) {

        global $database;
        $query = "";

        $query = "INSERT tbl_propriedades_loja ";
        $query .= "(";
        $query .= " prl_id_pub, ";
        $query .= " prl_informacao_nao_enviado, ";
        $query .= " prl_informacao_enviado, ";
        $query .= " prl_informacao_cancelado ";
        $query .= ") ";
        $query .= "VALUES ( ";
        $query .= " '" . Session::get("pub")["pub_id"] . "', ";
        $query .= " '{$propriedades["prl_informacao_nao_enviado"]}', ";
        $query .= " '{$propriedades["prl_informacao_enviado"]}', ";
        $query .= " '{$propriedades["prl_informacao_cancelado"]}' ";
        $query .= ") ";

        $database->query($query);
        return $database->insertId();
    }

    public static function updatePropriedades($propriedades) {
        global $database;
        $query = "";

        $query = "UPDATE tbl_propriedades_loja ";
        $query .= "SET ";
        $query .= " prl_informacao_nao_enviado = '{$propriedades["prl_informacao_nao_enviado"]}', ";
        $query .= " prl_informacao_enviado = '{$propriedades["prl_informacao_enviado"]}', ";
        $query .= " prl_informacao_cancelado = '{$propriedades["prl_informacao_cancelado"]}' ";
        $query .= "WHERE ";
        $query .= "prl_id = '{$propriedades["prl_id"]}'";
        
        return $database->update($query) == 1 ? true : false;
    }

}
