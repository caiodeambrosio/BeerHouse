<?php
$municipio_id = isset($municipio_id) ? $municipio_id : "";
if (isset($estado_id)) {
    $estado = Estado::getEstado($estado_id);
    $optionsMunicipio = Municipio::getMunicipiosByUF($estado["est_uf"]);
}
?>
<select name="municipio" id="txtMunicipio" class="form-control">
    <option value="">Escolha...</option>
    <?php
    for ($cmbIterator = 0; $cmbIterator < count($optionsMunicipio); $cmbIterator++) {
        ?>
        <option value="<?= $optionsMunicipio[$cmbIterator]["mun_id"] ?>" <?= ($municipio_id == $optionsMunicipio[$cmbIterator]["mun_id"]) ? "selected" : "" ?>><?= $optionsMunicipio[$cmbIterator]["mun_nome"] ?></option>
        <?php
    }
    ?>
</select>
