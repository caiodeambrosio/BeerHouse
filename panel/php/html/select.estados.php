<?php
$estado_id = isset($estado_id) ? $estado_id : "";
$optionsEstado = Estado::getEstados();
?>

<select name="estado" id="txtEstado" class="form-control">
    <option value="">Escolha...</option>
    <?php
    for ($cmbIterator = 0; $cmbIterator < count($optionsEstado); $cmbIterator++) {
        ?>
        <option value="<?= $optionsEstado[$cmbIterator]["est_id"] ?>" <?= ($estado_id == $optionsEstado[$cmbIterator]["est_id"]) ? "selected" : "" ?>><?= $optionsEstado[$cmbIterator]["est_nome"] ?></option>
        <?php
    }
    ?>
</select>
