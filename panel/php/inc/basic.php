<?php

require_once ("./../php/class/Database.class.php");
require_once ("./../php/class/Notify.class.php");
require_once ("./../php/class/Util.class.php");
require_once ("./../php/class/SystemMessage.class.php");
require_once ("./../php/class/Session.class.php");
require_once ("./../php/class/Upload.class.php");

require_once ("./php/sql/Admin.php");
require_once ("./php/sql/Cliente.php");
require_once ("./php/sql/Estado.php");
require_once ("./php/sql/Municipio.php");
require_once ("./php/sql/PropriedadesLoja.php");

require_once ("./../php/sql/Contato.php");
require_once ("./../php/sql/Evento.php");
require_once ("./../php/sql/ItemLoja.php");
require_once ("./../php/sql/Localidade.php");
require_once ("./../php/sql/Pub.php");
require_once ("./../php/sql/Login.php");





Session::start();
$database = new Database();
$notify = new Notify();

$page = Util::getCurrentPage();

if ($page == "gerenciar.login.php") {
    if (Login::logged()) {
        header("Location: index.php");
    }
} else { // Page different than "login.php"
    if (!Login::logged()) {
        header("Location: gerenciar.login.php");
    }
}

?>