<nav class="navbar navbar-expand navbar-dark" style="background-color: #3b1942; border-radius: 0 0 6px 6px;">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown2" aria-controls="navbarNavDropdown2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="#">&nbsp;</a>

    <div class="collapse navbar-collapse" id="navbarNavDropdown2">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="gerenciar.dashboard.php?pub_id=<?= Session::get("pub")["pub_id"] ?>"><span class="fa fa-th-large"></span> Dashboard</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="gerenciar.notifications.php"><span class="fa fa-bell"></span> Notificações</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="gerenciar.inbox.php"><span class="fa fa-envelope"></span> Inbox</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="gerenciar.offers.php">Ofertas</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="gerenciar.pagamentos.php">Pagamentos</a>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-globe"></span> Loja
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="gerenciar.storeOrders.php">Pedidos</a>
                    <a class="dropdown-item" href="gerenciar.storeItems.php">Itens</a>
                    <a class="dropdown-item" href="gerenciar.storeSettings.php">Propriedades</a>

                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-globe"></span> Website
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="gerenciar.pub.php"><span class="fa fa-cogs"></span>Configurações</a>
                    <a class="dropdown-item" href="gerenciar.sobre.php"><span class="fa fa-quote-right"></span>Sobre Nós</a>
                    <a class="dropdown-item" href="gerenciar.eventos.php"><span class="fa fa-bullhorn"></span>Eventos</a>
                    <a class="dropdown-item" href="gerenciar.faq.php"><span class="fa fa-question-circle"></span>FAQ</a>

                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-globe"></span> Other
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="gerenciar.regions.php">Regions</a>
                    <a class="dropdown-item" href="gerenciar.locations.php">Locations</a>
                    <a class="dropdown-item" href="gerenciar.coaches.php">Coaches & Trainers</a>
                    <a class="dropdown-item" href="gerenciar.teams.php">Teams</a>
                    <a class="dropdown-item" href="gerenciar.users.php">Users</a>
                    <a class="dropdown-item" href="gerenciar.players.php">Players</a>
                    <a class="dropdown-item" href="gerenciar.programs.php">Programs & Clinics</a>
                    <a class="dropdown-item" href="gerenciar.waivers.php">Waivers</a>
                    <a class="dropdown-item" href="gerenciar.history.php">Login History</a>
                    <a class="dropdown-item" href="gerenciar.systemSettings.php">System Settings</a>

                </div>
            </li>

        </ul>
        <ul class="navbar-nav">
            <li>
                <img src="./../img/loading.gif" id="SystemLoading">
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link" href="../" target="_blank"><span class="fa fa-external-link"></span> Open Website</a>
            </li>
        </ul>
    </div>
</nav>