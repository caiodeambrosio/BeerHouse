<div class="row justify-content-md-center">
    <div class="col-lg-2" style="padding: 25px 10px 25px 10px;">
        <?php if (isset($_SESSION["pub"])) {
            ?>
            <img src="./../img/repo/<?= $_SESSION["pub"]["pub_logo"] ?>" class="img-fluid">
        <?php } else { ?>
            <img src="./../img/logo.png" class="img-fluid">
        <?php } ?>
    </div>
</div>


<nav class="navbar navbar-expand navbar-dark  navbar-toggleable-md" style="background-color: #512c1a;">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown1" aria-controls="navbarNavDropdown1" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="#">&nbsp;</a>

    <div class="collapse navbar-collapse" id="navbarNavDropdown1">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php"><span class="fa fa-home"></span> Home</a>
            </li>                     
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa fa-beer"></span> Pubs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="gerenciar.pubs.php"><span class="fa fa- fa-cogs"></span> Gerenciar</a>
                    <div class="dropdown-divider"></div>
                    <?php
                    $pubs = Pub::getPubsByOwner();
                    for ($i = 0; $i < count($pubs); $i++) {
                        ?>
                        <a class="dropdown-item" href="gerenciar.pub.php?pub_id=<?= $pubs[$i]['pub_id'] ?>"><?= $pubs[$i]['pub_titulo'] ?></a>
                        <?php
                    }
                    ?>


                </div>
            </li>

        </ul>
        <ul class="navbar-nav">
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span id="LblUser">Hi, <?= Session::get("admin")["adm_nome"] ?></span> <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="gerenciar.logout.php">Log Out</a>
                </div>
            </li>
        </ul>
    </div>
</nav>