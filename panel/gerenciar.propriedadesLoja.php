<?php
require_once "./php/inc/basic.php";

$propriedades = PropriedadesLoja::getPropriedade();
$localidades = Localidade::getLocalidades();

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Edit": {
                $id = $_REQUEST["id"];
                $propriedades = PropriedadesLoja::getPropriedade();
                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_SETTINGS_UPDATE, Notify::NOTIFY_TYPE_SUCCESS);
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_SETTINGS_UPDATE, Notify::NOTIFY_TYPE_SUCCESS);
                }
            } break;
        case "Salvar": {

                $propriedades["prl_informacao_nao_enviado"] = $_REQUEST["informacao_nao_enviado"];
                $propriedades["prl_informacao_enviado"] = $_REQUEST["informacao_enviado"];
                $propriedades["prl_informacao_cancelado"] = $_REQUEST["informacao_cancelado"];

                $id = PropriedadesLoja::savePropriedades($propriedades);
                if (is_numeric($id)) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_SETTINGS_UPDATE, Notify::NOTIFY_TYPE_SUCCESS);
                } else {
                    $notify->set("Erro", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                }


                for ($i = 0; $i < count($_REQUEST["nome"]); $i++) {
                    $localidade["pll_nome"] = $_REQUEST["nome"][$i];
                    $localidade["pll_custo_envio"] = $_REQUEST["custo_envio"][$i];

                    if (Localidade::validate($localidade)) {
                        Localidade::saveLocalidade($id, $localidade);
                    }
                }
                header("Location: gerenciar.propriedadesLoja.php?action=Edit&id={$id}&saved");
            } break;
        case "Atualizar": {
            
                $propriedades["prl_id"] = $_REQUEST["id_propriedade"];
                $propriedades["prl_informacao_nao_enviado"] = $_REQUEST["informacao_nao_enviado"];
                $propriedades["prl_informacao_enviado"] = $_REQUEST["informacao_enviado"];
                $propriedades["prl_informacao_cancelado"] = $_REQUEST["informacao_cancelado"];

                if (!PropriedadesLoja::updatePropriedades($propriedades)) {
                    $notify->set("Erro", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                    break;
                }

                for ($i = 0; $i < count($_REQUEST["nome"]); $i++) {
                    $localidade["pll_id"] = $_REQUEST["id"][$i];
                    $localidade["pll_nome"] = $_REQUEST["nome"][$i];
                    $localidade["pll_custo_envio"] = $_REQUEST["custo_envio"][$i];

                    if (Localidade::validate($localidade)) {
                        Localidade::updateLocalidade($localidade);
                    } else {
                        $notify->set("Erro", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                        break;
                    }
                }

                $id = $propriedades["prl_id"];
                header("Location: gerenciar.propriedadesLoja.php?action=Edit&id={$id}&updated");
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
    </head>
    <body>
        <div class="container-fluid">
            <?php include "./php/inc/links.php" ?>
            <?php
            $section_title = "<span class='fa fa-cog'></span> Propriedades da Loja";
            include "./php/html/section.title.php";
            ?>
            <?php
            $notify->show();
            ?>
            <form method="POST" action="gerenciar.propriedadesLoja.php">
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-legend col-sm-4" style="text-align: right;"><b>Envio e Entrega</b></legend>
                        <div class="col-sm-8">

                            <div class="form-group">
                                <label for="txtLocalidade">Localidade <small>(opcional)</small> <a href="javascript:void(0);" id="btnAddLocalidade" class="btn btn-sm btn-secondary" style="margin-left: 10px;"><span class="fa fa-plus-circle cr-primary"></span></a></label>

                                <div class="form-relative" id="ctnAddLocalidade">
                                    <?php
                                    if ($propriedades != null && $localidades != null && count($localidades) > 0) {
                                        $html = "";
                                        for ($i = 0; $i < count($localidades); $i++) {
                                            $localidade = $localidades[$i];

                                            $html .= "<div class='form-group itmAddLocalidade' style='border-bottom: 1px dotted #ddd; padding: 0 0 15px 0;'>";
                                            $html .= "   <div class='row'>";
                                            if (PropriedadesLoja::existPropriedades()) {
                                                $html .= "       <input type='hidden' name='id[]' value='{$localidade["pll_id"]}'>";
                                            }
                                            $html .= "       <div class='col-lg-8'>";
                                            $html .= "           <small>Localidade</small>";
                                            $html .= "           <input type='text' class='form-control' name='nome[]' value='{$localidade["pll_nome"]}' aria-describedby='btnGroupAddon2'>";
                                            $html .= "       </div>";
                                            $html .= "       <div class='col-lg-2'>";
                                            $html .= "           <small>Custo de Envio</small>";
                                            $html .= "           <input type='text' class='form-control' name='custo_envio[]' value='{$localidade["pll_custo_envio"]}' aria-describedby='btnGroupAddon2'>";
                                            $html .= "       </div>";
                                            $html .= "       <div class='col-lg-2'>";
                                            $html .= "       <small>&nbsp;</small>";
                                            $html .= "           <a href='javascript:void(0);' class='btn btn-secondary btnRemLocalidade' style='width: 100%;'><span class='fa fa-minus-circle cr-danger'></span></a>";
                                            $html .= "     </div>";
                                            $html .= "  </div>";
                                            $html .= "</div>";
                                        }
                                        echo $html;
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type='hidden' name='id_propriedade' value='<?= $propriedades["prl_id"] ?>'>
                            </div>
                            <div class="form-group">
                                <label for="txtInformacaoNaoEnviado">Informação Para o Consumidor Quando o Status for "Não Enviado"</label>
                                <textarea class="form-control" id="txtInformacaoNaoEnviado" name="informacao_nao_enviado" placeholder="" rows="5"><?= isset($propriedades["prl_informacao_nao_enviado"]) ? $propriedades["prl_informacao_nao_enviado"] : "" ?></textarea>
                                <!--<small id="emailHelp" class="form-text text-muted">Shipping Additional Information regarding Delivery Time, Contact for dispute resolution, etc. This information will be presented on Checkout and will be Mailed to the customers once they place an order.</small>-->
                            </div>
                            <div class="form-group">
                                <label for="txtInformacaoEnviado">Informação Para o Consumidor Quando o Status for "Enviado"</label>
                                <textarea class="form-control" id="txtInformacaoEnviado" name="informacao_enviado" placeholder="" rows="5"><?= isset($propriedades["prl_informacao_enviado"]) ? $propriedades["prl_informacao_enviado"] : "" ?></textarea>
                                <!--<small id="emailHelp" class="form-text text-muted">Shipping Additional Information regarding Delivery Time, Contact for dispute resolution, etc. This information will be presented on Checkout and will be Mailed to the customers once they place an order.</small>-->
                            </div>
                            <div class="form-group">
                                <label for="txtInformacaoCancelado">Informação Para o Consumidor Quando o Status for "Cancelado"</label>
                                <textarea class="form-control" id="txtInformacaoCancelado" name="informacao_cancelado" placeholder="" rows="5"><?= isset($propriedades["prl_informacao_cancelado"]) ? $propriedades["prl_informacao_cancelado"] : "" ?></textarea>
                                <!--<small id="emailHelp" class="form-text text-muted">Shipping Additional Information regarding Delivery Time, Contact for dispute resolution, etc. This information will be presented on Checkout and will be Mailed to the customers once they place an order.</small>-->
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="row">
                    <legend class="col-form-legend col-sm-4">&nbsp;</legend>
                    <div class="col-sm-8">
                        <input type="submit" name="action" value="<?= !PropriedadesLoja::existPropriedades() ? "Salvar" : "Atualizar" ?>" class="btn btn-primary btn-block cursor-pointer"></input>
                    </div>
                </div>

            </form>

        </div>

    </body>
</html>


<script>
    $(function () {

        $(document).on("click", "#btnAddLocalidade", function () {
            var html = "";

            html += "<div class='form-group itmAddLocalidade' style='border-bottom: 1px dotted #ddd; padding: 0 0 15px 0;'>";
            html += "   <div class='row'>";
            html += "       <div class='col-lg-8'>";
            html += "           <small>Localidade</small>";
            html += "           <input type='text' class='form-control' name='nome[]' value='' aria-describedby='btnGroupAddon2'>";
            html += "       </div>";
            html += "       <div class='col-lg-2'>";
            html += "           <small>Custo de Envio</small>";
            html += "           <input type='text' class='form-control' name='custo_envio[]' value='' aria-describedby='btnGroupAddon2'>";
            html += "       </div>";
            html += "       <div class='col-lg-2'>";
            html += "       <small>&nbsp;</small>";
            html += "           <a href='javascript:void(0);' class='btn btn-secondary btnRemLocalidade' style='width: 100%;'><span class='fa fa-minus-circle cr-danger'></span></a>";
            html += "     </div>";
            html += "  </div>";
            html += "</div>";

            $("#ctnAddLocalidade").append(html);
        });

        /* #btnAddLocalidade - Removes a Localidade */
        $(document).on("click", ".btnRemLocalidade", function () {
            $(this).closest(".itmAddLocalidade").fadeOut(function () {
                $(this).remove();
            });
        });

        /* #btnAddLocalidade - Adds a Localidade */
//        $(document).on("click", "#btnAddLocalidade", function () {
//            var html = "";
//
//            html += "<div class='form-group itmAddLocalidade' style='border-bottom: 1px dotted #ddd; padding: 0 0 15px 0;'>";
//            html += "   <div class='row'>";
//            html += "     <div class='col-lg-10'>";
//            html += "        <small>Localidade</small> <input type='text' class='form-control fldLocation' name='date[]' value='' placeholder='00/00/0000' aria-describedby='btnGroupAddon2'>";
//            html += "     </div>";
//            html += "     <div class='col-lg-2'>";
//            html += "        <small>&nbsp;</small> <a href='javascript:void(0);' class='btn btn-secondary btnRemLocalidade' style='width: 100%;'><span class='fa fa-minus-circle cr-danger'></span></a>";
//            html += "     </div>";
//            html += "  </div>";
//            html += "   <div class='row' style='margin-top: 5px;'>";
//            html += "     <div class='col-lg-10'>";
//            html += "        <div class='row'>";
//            html += "           <div class='col-lg-3'>";
//            html += "              <small>Starts</small> <input type='text' class='form-control fldTime' name='time_start[]' placeholder='00:00'>";
//            html += "           </div>";
//            html += "           <div class='col-lg-3'>";
//            html += "              <small>&nbsp;</small>";
//            html += "              <select class='form-control' name='time_start_meridian[]'>";
//            html += "                 <option value='am'>am</option>";
//            html += "                 <option value='pm'>pm</option>";
//            html += "              </select>";
//            html += "           </div>";
//            html += "           <div class='col-lg-3'>";
//            html += "              <small>Ends</small> <input type='text' class='form-control fldTime' name='time_end[]' placeholder='00:00'>";
//            html += "           </div>";
//            html += "           <div class='col-lg-3'>";
//            html += "              <small>&nbsp;</small>";
//            html += "              <select class='form-control' name='time_end_meridian[]'>";
//            html += "                 <option value='am'>am</option>";
//            html += "                 <option value='pm'>pm</option>";
//            html += "              </select>";
//            html += "           </div>";
//            html += "        </div>";
//            html += "     </div>";
//            html += "     <div class='col-lg-2'>";
//            html += "        &nbsp;";
//            html += "     </div>";
//            html += "   </div>";
//            html += "</div>";
//
//            $("#ctnAddLocalidade").append(html);
//        });

    });
</script>