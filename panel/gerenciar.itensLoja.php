<?php
require_once "./php/inc/basic.php";

$itemLoja = null;
$modalAddFlag = false;
if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];
                $itemLoja = ItemLoja::getItemLoja($id);
                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {
                $modalAddFlag = true;
                $itemLoja["itl_titulo"] = $_REQUEST["titulo"];
                $itemLoja["itl_descricao"] = $_REQUEST["descricao"];
                $itemLoja["itl_valor"] = $_REQUEST["valor"];
                $itemLoja["itl_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $itemLoja["itl_imagem"] = "";

                $imagem = Upload::upload("imagem", "./../img/repo/", true);

                if (ItemLoja::validateSaveItemLoja($itemLoja)) {
                    $id = ItemLoja::saveItemLoja($itemLoja, $imagem);
                    if (is_numeric($id)) {
                        header("location: gerenciar.itensLoja.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                    }
                }
            } break;
        case "Update": {
                $modalAddFlag = true;
                $itemLoja["itl_id"] = $_REQUEST["id"];
                $itemLoja["itl_titulo"] = $_REQUEST["titulo"];
                $itemLoja["itl_descricao"] = $_REQUEST["descricao"];
                $itemLoja["itl_valor"] = $_REQUEST["valor"];
                $itemLoja["itl_status"] = isset($_REQUEST["ativo"]) ? 1 : 0;
                $itemLoja["itl_imagem"] = "";

                $imagem = Upload::upload("imagem", "./../img/repo/", true);

                if (ItemLoja::validateUpdateItemLoja($itemLoja)) {
                    $return = ItemLoja::updateItemLoja($itemLoja);
                    if ($return) {
                        $id = $itemLoja["itl_id"];
                        header("location: gerenciar.itensLoja.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>

        <div class="container-fluid">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-shopping-bag'></span> Itens da Loja";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 5%;">Imagem</th>
                        <th style="width: 20%;">Titulo</th>
                        <th style="width: 20%;">Descrição</th>
                        <th style="width: 15%;">Valor</th>
                        <th style="width: 15%;">Status</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $table = ItemLoja::getItensLoja();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td><?= $table[$i]["itl_id"] ?></td>
                            <td>
                                <?php if (!empty($table[$i]["itl_imagem"])) { ?>
                                    <img src="./../img/repo/<?= $table[$i]["itl_imagem"] ?>" class="img-fluid rounded">
                                <?php } ?>
                            </td>
                            <td><?= $table[$i]["itl_titulo"] . " " . $table[$i]["itl_descricao"] ?></td>
                            <td><?= $table[$i]["itl_descricao"] ?></td>
                            <td><?= $table[$i]["itl_valor"] ?></td>
                            <td><?= $table[$i]["itl_status"] == 1 ? "<span class='badge badge-success'>Ativo</span>" : "<span class='badge badge-danger'>Inativo</span>" ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?= $table[$i]["itl_id"] ?>">
                                    <button type="submit" class="btn btn-primary" name="action" value="Edit">Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->

        <form method="POST" action="gerenciar.itensLoja.php" enctype="multipart/form-data">
            <input type="hidden" name="action" value=""> 
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $itemLoja == null ? "Novo" : "Editar" ?> Item da Loja</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <?php if (is_array($itemLoja) && isset($itemLoja["itl_id"])) { ?>
                                <input type="hidden" name="id" value="<?= ($itemLoja != null) ? $itemLoja["itl_id"] : "" ?>">
                            <?php } ?>
                            <legend>Informações do Item</legend>
                            <div class="form-group">
                                <label for="txtTitulo">Titulo<span class="required">*</span></label>
                                <input type="text" class="form-control" name="titulo" id="txtTitulo" value="<?= ($itemLoja != null) ? $itemLoja["itl_titulo"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtDescricao">Descrição <span class="required">*</span></label>
                                <input type="text" class="form-control" name="descricao" id="txtDescricao" placeholder="" value="<?= ($itemLoja != null) ? $itemLoja["itl_descricao"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtValor">Valor</label>
                                <input type="text" class="form-control" name="valor" id="txtValor" value="<?= ($itemLoja != null) ? $itemLoja["itl_valor"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="fileItemImagem">Imagem do Produto</label>
                                <input type="file" class="form-control-file" name="imagem">
                                <?php if (strlen($itemLoja["itl_imagem"]) > 0) { ?>
                                    <div style="margin-top: 15px;">
                                        <img src="./../img/repo/<?= $itemLoja != null ? $itemLoja["itl_imagem"] : "" ?>"/> 
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="status" <?= $itemLoja != null ? ($itemLoja["itl_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Ativo</span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($itemLoja["itl_id"]) ? "Salvar" : "Update";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>


<script>
    $(function () {
<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
            $("#txtDataNascimento").mask("99/99/9999");
<?php } ?>
        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.itensLoja.php?action=Add";
        });

        $(document).on("click", "#BtnExibirSenha", function () {
            if ($("#BtnExibirSenha").hasClass("status")) {
                $("#txtPassword").prop("type", "text");
            } else {
                $("#txtPassword").prop("type", "password");
            }
        });

        /* Event on txtEstado */
        $(document).on("change", "#txtEstado", function () {
            var estado = $(this).val();
            $("#ctnMunicipio").html("<img src='./../img/loading.gif' class='loading-gif'>");
            $.post("./php/ajax/get.html.select.municipios.php", {
                estado: estado
            }, function (r) {
                $("#ctnMunicipio").html(r);
            });
        });
    });
</script>