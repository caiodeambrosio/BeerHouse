<?php
require_once "./php/inc/basic.php";

$localidade = null;
$modalAddFlag = false;
if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];
                $localidade = Localidade::getLocalidade($id);
                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {
                $modalAddFlag = true;
                $localidade["loc_nome"] = $_REQUEST["nome"];
                $localidade["loc_descricao"] = $_REQUEST["descricao"];
                $localidade["loc_status"] = isset($_REQUEST["status"]) ? 1 : 0;

                if (Localidade::validateSaveLocalidade($localidade)) {
                    $id = Localidade::saveLocalidade($localidade);
                    if (is_numeric($id)) {
                        header("location: gerenciar.localidades.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                    }
                }
            } break;
        case "Update": {
                $modalAddFlag = true;
                $localidade["loc_id"] = $_REQUEST["id"];
                $localidade["loc_nome"] = $_REQUEST["nome"];
                $localidade["loc_descricao"] = $_REQUEST["descricao"];
                $localidade["loc_valor"] = $_REQUEST["valor"];
                $localidade["loc_status"] = isset($_REQUEST["ativo"]) ? 1 : 0;
                $localidade["loc_imagem"] = "";

                $imagem = Upload::upload("imagem", "./../img/repo/", true);

                if (Localidade::validateUpdateLocalidade($localidade)) {
                    $return = Localidade::updateLocalidade($localidade);
                    if ($return) {
                        $id = $localidade["loc_id"];
                        header("location: gerenciar.localidades.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                        ;
                        ;
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>

        <div class="container-fluid">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-shopping-bag'></span> Itens da Loja";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 5%;">Imagem</th>
                        <th style="width: 20%;">Titulo</th>
                        <th style="width: 20%;">Descrição</th>
                        <th style="width: 15%;">Valor</th>
                        <th style="width: 15%;">Status</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $table = Localidade::getItensLoja();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td><?= $table[$i]["loc_id"] ?></td>
                            <td>
                                <?php if (!empty($table[$i]["loc_imagem"])) { ?>
                                    <img src="./../img/repo/<?= $table[$i]["loc_imagem"] ?>" class="img-fluid rounded">
                                <?php } ?>
                            </td>
                            <td><?= $table[$i]["loc_nome"] . " " . $table[$i]["loc_descricao"] ?></td>
                            <td><?= $table[$i]["loc_descricao"] ?></td>
                            <td><?= $table[$i]["loc_valor"] ?></td>
                            <td><?= $table[$i]["loc_status"] == 1 ? "<span class='badge badge-success'>Ativo</span>" : "<span class='badge badge-danger'>Inativo</span>" ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?= $table[$i]["loc_id"] ?>">
                                    <button type="submit" class="btn btn-primary" name="action" value="Edit">Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->

        <form method="POST" action="gerenciar.localidades.php" enctype="multipart/form-data">
            <input type="hidden" name="action" value=""> 
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $localidade == null ? "Novo" : "Editar" ?> Item da Loja</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <?php if (is_array($localidade) && isset($localidade["loc_id"])) { ?>
                                <input type="hidden" name="id" value="<?= ($localidade != null) ? $localidade["loc_id"] : "" ?>">
                            <?php } ?>
                            <legend>Informações do Item</legend>
                            <div class="form-group">
                                <label for="txtTitulo">Titulo<span class="required">*</span></label>
                                <input type="text" class="form-control" name="nome" id="txtTitulo" value="<?= ($localidade != null) ? $localidade["loc_nome"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtDescricao">Descrição <span class="required">*</span></label>
                                <input type="text" class="form-control" name="descricao" id="txtDescricao" placeholder="" value="<?= ($localidade != null) ? $localidade["loc_descricao"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="txtValor">Valor</label>
                                <input type="text" class="form-control" name="valor" id="txtValor" value="<?= ($localidade != null) ? $localidade["loc_valor"] : "" ?>" autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="fileItemImagem">Imagem do Produto</label>
                                <input type="file" class="form-control-file" name="imagem">
                                <?php if (strlen($localidade["loc_imagem"]) > 0) { ?>
                                    <div style="margin-top: 15px;">
                                        <img src="./../img/repo/<?= $localidade != null ? $localidade["loc_imagem"] : "" ?>"/> 
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="status" <?= $localidade != null ? ($localidade["loc_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Ativo</span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($localidade["loc_id"]) ? "Salvar" : "Update";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>


<script>
    $(function () {
<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
            $("#txtDataNascimento").mask("99/99/9999");
<?php } ?>
        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.localidades.php?action=Add";
        });

        $(document).on("click", "#BtnExibirSenha", function () {
            if ($("#BtnExibirSenha").hasClass("status")) {
                $("#txtPassword").prop("type", "text");
            } else {
                $("#txtPassword").prop("type", "password");
            }
        });

        /* Event on txtEstado */
        $(document).on("change", "#txtEstado", function () {
            var estado = $(this).val();
            $("#ctnMunicipio").html("<img src='./../img/loading.gif' class='loading-gif'>");
            $.post("./php/ajax/get.html.select.municipios.php", {
                estado: estado
            }, function (r) {
                $("#ctnMunicipio").html(r);
            });
        });
    });
</script>