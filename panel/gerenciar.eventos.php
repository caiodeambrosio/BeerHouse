<?php
require_once "./php/inc/basic.php";

$evento = null;
$modalAddFlag = false;


if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];
                $evento = Evento::getEvento($id);
                $evento["eve_hora_inicio"] = Util::getTimeToShow($evento["eve_hora_inicio"]);
                $evento["eve_hora_fim"] = Util::getTimeToShow($evento["eve_hora_fim"]);
                
                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {
                $modalAddFlag = true;
                $evento["eve_titulo"] = $_REQUEST["titulo"];
                $evento["eve_descricao"] = $_REQUEST["descricao"];
                $evento["eve_valor"] = Util::getValueToSave($_REQUEST["valor"]);
                $evento["eve_data"] = $_REQUEST["data"];
                $evento["eve_hora_inicio"] = $_REQUEST["hora_inicio"];
                $evento["eve_hora_fim"] = $_REQUEST["hora_fim"];
                $evento["eve_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $evento["eve_banner"] = "";

                $banner = Upload::upload("banner", "./../img/repo/", true);

                if (Evento::validateSaveEvento($evento)) {
                    $id = Evento::saveEvento($evento, $banner);
                    if (is_numeric($id)) {
                        header("location: gerenciar.eventos.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR);
                    }
                }
            } break;
        case "Atualizar": {
                $modalAddFlag = true;
                $evento["eve_id"] = $_REQUEST["id"];
                $evento["eve_titulo"] = $_REQUEST["titulo"];
                $evento["eve_descricao"] = $_REQUEST["descricao"];
                $evento["eve_valor"] = $_REQUEST["valor"];
                $evento["eve_data"] = $_REQUEST["data"];
                $evento["eve_hora_inicio"] = $_REQUEST["hora_inicio"];
                $evento["eve_hora_fim"] = $_REQUEST["hora_fim"];
                $evento["eve_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $evento["eve_banner"] = "";

                $banner = Upload::upload("banner", "./../img/repo/", true);

                if (Evento::validateUpdateEvento($evento)) {
                    $return = Evento::updateEvento($evento, $banner);
                    if ($return) {
                        $id = $evento["eve_id"];
                        header("location: gerenciar.eventos.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>

        <div class="container-fluid">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-bullhorn'></span> Eventos";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 20%;">Titulo</th>
                        <th style="width: 20%;">Descrição</th>
                        <th style="width: 15%;">Data</th>
                        <th style="width: 10%;">Valor</th>
                        <th style="width: 10%;">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $table = Evento::getEventos();
                    
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td><?= $table[$i]["eve_titulo"] ?></td>
                            <td><?= $table[$i]["eve_descricao"] ?></td>
                            <td><?= Util::getDateToShow($table[$i]["eve_data"]) . " " . Util::getTimeToShow($table[$i]["eve_hora_inicio"]) . " - " . Util::getTimeToShow($table[$i]["eve_hora_fim"]) ?> </td>
                            <td class="fldPrice"><?= $table[$i]["eve_valor"] ?></td>
                            <td><?= $table[$i]["eve_status"] == 1 ? "<span class='badge badge-success'>Ativo</span>" : "<span class='badge badge-danger'>Inativo</span>" ?></td>
                            <td>
                                <form method="POST">
                                    <input type="hidden" name="id" value="<?= $table[$i]["eve_id"] ?>">
                                    <button type="submit" class="btn btn-primary" name="action" value="Edit">Edit</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->

        <form method="POST" action="gerenciar.eventos.php" enctype="multipart/form-data">
            <input type="hidden" name="action" value=""> 
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title"><?= $evento == null ? "Novo" : "Editar" ?> Evento</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                        </div>

                        <div class="modal-body">

                            <?php
                            $notify->show("modalAdd");
                            ?>

                            <?php if (is_array($evento) && isset($evento["eve_id"])) { ?>
                                <input type="hidden" name="id" value="<?= ($evento != null) ? $evento["eve_id"] : "" ?>">
                            <?php } ?>

                            <legend>Informações do Evento</legend>
                            <div class="form-group">
                                <label for="txtTitulo">Titulo<span class="required">*</span></label>
                                <input type="text" class="form-control" name="titulo" id="txtTitulo" value="<?= ($evento != null) ? $evento["eve_titulo"] : "" ?>" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="txtDescricao">Descrição <span class="required">*</span></label>
                                <input type="text" class="form-control" name="descricao" id="txtDescricao" placeholder="" value="<?= ($evento != null) ? $evento["eve_descricao"] : "" ?>" autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label for="txtValor">Valor</label>
                                <input type="text" class="form-control fldPrice" name="valor" id="txtValor" value="<?= ($evento != null) ? $evento["eve_valor"] : "" ?>" autocomplete="off">
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="txtData">Data <span class="required">*</span></label>
                                        <input type="date" class="form-control" name="data" id="txtData" value="<?= $evento != null ? $evento["eve_data"] : "" ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="txtHoraInicio">Hora Inicio <span class="required">*</span></label>
                                        <input type="time" class="form-control" name="hora_inicio" id="txtHoraInicio" value="<?= $evento != null ? $evento["eve_hora_inicio"] : "" ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="txtHora_Fim">Hora Fim <span class="required">*</span></label>
                                        <input type="time" class="form-control fldDate" name="hora_fim" id="txtHora_Fim" value="<?= $evento != null ? $evento["eve_hora_fim"] : "" ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="fileEventoBanner">Evento Banner</label>
                                <?php if (strlen($evento["eve_banner"]) > 0) { ?>
                                    <div class="card" style="margin-bottom: 15px;">

                                        <div class="row justify-content-md-center">
                                            <div class="col-lg-12">
                                                <div style="margin: 15px 15px 15px 15px;">
                                                    <img class="card-img-top" src="./../img/repo/<?= $evento != null ? $evento["eve_banner"] : "" ?>"/> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <label class="custom-file" style="display:block">
                                    <input type="file" id="txtBanner" name="banner" class="custom-file-input">
                                    <span class="custom-file-control"><?= $evento != null ? $evento["eve_banner"] : "" ?></span>
                                </label>
                            </div>

                            <div class="form-check">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="status" <?= $evento != null ? ($evento["eve_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Ativo</span>
                                </label>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($evento["eve_id"]) ? "Salvar" : "Atualizar";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>


<script>
    $(function () {
<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");

<?php } ?>
        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.eventos.php?action=Add";
        });

        $(".custom-file-input").on("change", function () {
            let fileName = $(this).val().split("\\").pop();
            $(this).next(".custom-file-control").addClass("selected").html(fileName);
        });
    });
</script>