<?php
require_once "./php/inc/basic.php";

$pub = null;
$modalAddFlag = false;

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $id = $_REQUEST["id"];
                $pub = Pub::getPub($id);

                header("Location: gerenciar.pub.php?pub_id={$id}");
            } break;
        case "Salvar": {

                $modalAddFlag = true;
                $pub["pub_titulo"] = $_REQUEST["nome"];
                $pub["pub_descricao"] = $_REQUEST["descricao"];
                $pub["pub_website"] = $_REQUEST["website"];
                $pub["pub_email"] = $_REQUEST["email"];
                $pub["pub_facebook"] = $_REQUEST["facebook"];
                $pub["pub_twitter"] = $_REQUEST["twitter"];
                $pub["pub_instagram"] = $_REQUEST["instagram"];
                $pub["pub_youtube"] = $_REQUEST["youtube"];
                $pub["pub_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $pub["pub_logo"] = "";
                $imagem = Upload::upload("imagem", "./../img/repo/", true);

                if (Pub::validateSavePub($pub)) {
                    $id = Pub::savePub($pub, $imagem);
                    if (is_numeric($id)) {
                        header("Location: gerenciar.pubs.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    }
                }
            } break;
        case "Update": {
                $modalAddFlag = true;

                $pub["pub_id"] = $_REQUEST["id"];
                $pub["pub_titulo"] = $_REQUEST["nome"];
                $pub["pub_descricao"] = $_REQUEST["descricao"];
                $pub["pub_website"] = $_REQUEST["website"];
                $pub["pub_email"] = $_REQUEST["email"];
                $pub["pub_facebook"] = $_REQUEST["facebook"];
                $pub["pub_twitter"] = $_REQUEST["twitter"];
                $pub["pub_instagram"] = $_REQUEST["instagram"];
                $pub["pub_youtube"] = $_REQUEST["youtube"];
                $pub["pub_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $pub["pub_logo"] = "";

                $imagem = Upload::upload("imagem", "./../img/repo/", true);

                if (Pub::validateUpdatePub($pub)) {
                    $return = Pub::updatePub($pub, $imagem);

                    if ($return) {
                        $id = $pub["pub_id"];
                        header("Location: gerenciar.pubs.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>
        <div class="container">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-beer'></span> Pubs";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead class="tblHead">
                    <tr>
                        <th style="width: 5%;">
                            <div class="dropdown">
                                <button class="btn btn-sm  btn-outline-dark dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="fa fa-fa-caret-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="javascript:void(0);" id="btnAdd">Novo Item</a>
                                </div>
                            </div>
                        </th>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 10%;">Logo</th>
                        <th style="">Nome</th>
                        <th style="width: 5%;">Active</th>
                        <th style="width: 15%;" colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody class="tblBody">
                    <?php
                    $table = Pub::getPubs();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td><?= sprintf("%09d", $table[$i]["pub_id"]) ?></td>
                            <td>
                                <?php if (!empty($table[$i]["pub_logo"])) { ?>
                                    <img src="./../img/repo/<?= $table[$i]["pub_logo"] ?>" class="img-fluid">
                                <?php } ?>
                            </td>
                            <td>
                                <b><?= $table[$i]["pub_titulo"] ?></b><br>
                                <?= $table[$i]["pub_website"] ?>
                            </td>
                            <td><?= $table[$i]["pub_status"] == 1 ? "<span class='badge badge-success'>Active</span>" : "<span class='badge badge-default'>Instatus</span>" ?></td>
                            <td>
                                <form method="GET" action="gerenciar.pubs.php" style="">
                                    <button type="submit" class="btn btn-sm btn-block btn-primary cursor-pointer" name="action" value="Edit">Edit</button>
                                    <input type="hidden" name="id" value="<?= $table[$i]["pub_id"] ?>">
                                </form>
                            </td>
                            <td>
                                <a href="gerenciar.dashboard.php?pub_id=<?= $table[$i]["pub_id"] ?>" class="btn btn-sm btn-block btn-info cursor-pointer" >Dashboard</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
        <!-- Modal -->
        <form method="POST" action="gerenciar.pubs.php" enctype="multipart/form-data">
            <input type="hidden" name="action" value="">
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?= $pub == null && !isset($pub["pub_id"]) ? "Add" : "Edit" ?> Pub</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <?php if (is_array($pub) && isset($pub["pub_id"])) { ?>
                                <div class="form-group">
                                    <label for="txtNome">ID</label>
                                    <input type="hidden" name="id" value="<?= $pub != null ? $pub["pub_id"] : "" ?>">
                                    <input type="text" class="form-control" name="id" id="txtID" placeholder="" value="<?= $pub != null ? $pub["pub_id"] : "" ?>" disabled>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="txtNome">Nome</label>
                                <input type="text" class="form-control" name="nome" id="txtNome" placeholder="" value="<?= $pub != null ? $pub["pub_titulo"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtDescricao">Descricao</label>
                                <textarea class="form-control" name="descricao" id="txtDescricao" rows="4"><?= $pub != null ? $pub["pub_descricao"] : "" ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtWebsite">Website</label>
                                <input type="text" class="form-control" name="website" id="txtWebsite" placeholder="" value="<?= $pub != null ? $pub["pub_website"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtEmail">Email Address</label>
                                <input type="text" class="form-control" name="email" id="txtEmail" placeholder="" value="<?= $pub != null ? $pub["pub_email"] : "" ?>">
                            </div>
                            <div class="form-group">
                                <label for="txtFacebook">Facebook</label>
                                <div class="input-group">
                                    <span class="input-group-addon fa fa-facebook" id="facebook"></span>
                                    <input type="text" class="form-control" placeholder="" name="facebook" id="txtFacebook" aria-describedby="facebook" value="<?= $pub != null ? $pub["pub_facebook"] : "" ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtTwitter">Twitter</label>
                                <div class="input-group">
                                    <span class="input-group-addon fa fa-twitter" id="twitter"></span>
                                    <input type="text" class="form-control" placeholder="" name="twitter" id="txtTwitter" aria-describedby="twitter" value="<?= $pub != null ? $pub["pub_twitter"] : "" ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtInstagram">Instagram</label>
                                <div class="input-group">
                                    <span class="input-group-addon fa fa-instagram" id="instagram"></span>
                                    <input type="text" class="form-control" placeholder="" name="instagram" id="txtInstagram" aria-describedby="instagram" value="<?= $pub != null ? $pub["pub_instagram"] : "" ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtYoutube">Youtube</label>
                                <div class="input-group">
                                    <span class="input-group-addon fa fa-youtube" id="youtube"></span>
                                    <input type="text" class="form-control" placeholder="" name="youtube" id="txtYoutube" aria-describedby="youtube" value="<?= $pub != null ? $pub["pub_youtube"] : "" ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="filePubLogo">Pub Logo</label>
                                <input type="file" class="form-control-file" name="imagem">
                                <?php if (strlen($pub["pub_logo"]) > 0) { ?>
                                    <div style="margin-top: 15px;">
                                        <img src="./../img/repo/<?= $pub != null ? $pub["pub_logo"] : "" ?>"/> 
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="status" <?= $pub != null ? ($pub["pub_status"] == 1 ? "checked" : "") : "checked" ?>>
                                    Active
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <?php
                            $txtButton = !isset($pub["pub_id"]) ? "Salvar" : "Update";
                            ?>
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnSalvar" value="<?= $txtButton ?>"><?= $txtButton ?></button>
                            <button type="button" class="btn btn-secondary cursor-pointer" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>


<script>
    $(function () {

<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
<?php } ?>

        $(document).on("click", "#btnAdd", function () {
            document.location.href = "gerenciar.pubs.php?action=Add";
        });
    });
</script>