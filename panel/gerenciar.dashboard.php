<?php
require_once "./php/inc/basic.php";

$id = isset($_REQUEST["pub_id"]) ? $_REQUEST["pub_id"] : Session::get("pub")["pub_id"];
$pub = Pub::getPub($id);

Session::un_set("pub");
Session::set("pub", $pub);
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
    </head>
    <body>
        <div class="container-fluid">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-th-large'></span> Dashboard";
            include "./php/html/section.title.php";
            ?>

            <div class="row">


                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-send"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.ofertas.php">Ofertas</a></div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-cog"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.propriedadesLoja.php">Propriedades da Loja</a></div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-group"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.administradores.php">Administradores</a></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-key"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.permissoes.php">Permissões</a></div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-shopping-cart"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.pedidos.php">Pedidos da Loja</a></div>
                    </div>
                </div>


                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-calendar-check-o"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.pagamentos.php">Pagamentos</a></div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-cogs"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.desenvolvedor.php">Ferramentas do Desenvolvedor</a></div>
                    </div>
                </div>
            </div>


            <div class="section-title">
                <h5><b style="margin-left: 10px;">Testar</b></h5>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-group"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.clientes.php">Clientes</a></div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-shopping-bag"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.itensLoja.php">Itens da Loja</a></div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-bullhorn"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.eventos.php">Eventos</a></div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="metro-box metro-box-4 BtnMetro">
                        <div class="metro-box-icon"><span class="fa fa-envelope-o"></span></div>
                        <div class="metro-box-text"><a href="gerenciar.inbox.php">Contatos Inbox</a></div>
                    </div>
                </div>


            </div>

            <div class="section-title">
                <h5><b style="margin-left: 10px;">Prontos</b></h5>
            </div>

            <div class="row">


            </div>
        </div>

    </body>
</html>


<script>
    $(function () {

        $(".BtnMetro").click(function () {
            document.location.href = $(this).find("a").attr("href");
        });

    });
</script>