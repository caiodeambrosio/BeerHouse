<?php
require_once "./php/inc/basic.php";

$contato = null;
$modalAddFlag = false;

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Edit": {
                $modalAddFlag = true;
                $id = $_REQUEST["id"];
                $contato = Contato::getContato($id);

                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMensagem::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Atualizar": {
                $modalAddFlag = true;
                $contato = Contato::getContato($_REQUEST["id"]);
                $contato["con_resposta"] = $_REQUEST["resposta"];

                if (Contato::validateResposta($contato)) {
                    $return = Contato::updateContato($contato);

                    if ($return) {
                        $id = $contato["con_id"];

//                        SystemNotifications::saveNotification_ContatoRespondido($contato["con_id"], $contato["con_nome"], $contato["con_email"], $contato["con_assunto"]);
//                        $email = new Email();
//                        $email->sendToCustomer_ContatoMensagemRespondido($contato["con_nome"], $contato["con_email"], $contato["con_assunto"], $contato["con_mensagem"], $contato["con_resposta"]);
//                        $email->sendToAdmin_ContatoMensagemRespondido($contato["con_nome"], $contato["con_email"], $contato["con_telefone"], $contato["con_assunto"], $contato["con_mensagem"], $contato["con_resposta"]);

                        header("Location: gerenciar.inbox.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro", SystemMensagem::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
    </head>
    <body>

        <div class="container-fluid">
            <?php include "./php/inc/links.php" ?>
            <?php // include "./php/inc/notify.php" ?>
            <?php
            $section_title = "<span class='fa fa-envelope'></span> Contatos Inbox";
            include "./php/html/section.title.php";
            ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width: 5%;">ID</th>
                        <th style="width: 20%;">Origem</th>
                        <th style="width: 55%;">Mensagem</th>
                        <th style="width: 10%;">Status</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $table = Contato::getContatos();
                    for ($i = 0; $i < count($table); $i++) {
                        ?>
                        <tr>
                        <tr>

                            <td><?= $table[$i]["con_id"] ?></td>
                            <td>
                                <b><?= $table[$i]["con_nome"] ?></b><br>
                                <a href="mailto:<?= $table[$i]["con_email"] ?>"><?= $table[$i]["con_email"] ?></a>
                            </td>
                            <td>
                                <b><?= $table[$i]["con_assunto"] ?></b><br>
                                <p><?= $table[$i]["con_mensagem"] ?></p>
                            </td>
                            <td><?= $table[$i]["con_status"] == 1 ? "<span class='badge badge-pill badge-success'>Respondido</span>" : "<span class='badge badge-pill badge-danger'>Sem Resposta</span>" ?></td>
                            <td>
                                <form method="GET" action="gerenciar.inbox.php">
                                    <input type="hidden" name="id" value="<?= $table[$i]["con_id"] ?>">
                                    <button type="submit" class="btn btn-primary cursor-pointer" name="action" value="Edit"><?= $table[$i]["con_status"] == 1 ? "View" : "Edit" ?></button>
                                </form>
                            </td>
                        </tr>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>

        </div>

        <!-- Modal -->

        <form method="POST" enctype="multipart/form-data">
            <input type="hidden" name="action" value="">
            <div class="modal fade" id="modalAdd">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Mensagem Inbox #<?= $contato["con_id"] ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <?php
                            $notify->show("modalAdd");
                            ?>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            <b>Para</b><br>
                                            <?= $contato["con_nome"] ?><br> 
                                            <a href="mailto:<?= $contato["con_email"] ?>"><?= $contato["con_email"] ?></a><br>
                                            <a href="callto:<?= $contato["con_telefone"] ?>"><?= $contato["con_telefone"] ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Assunto</b><br>
                                            <?= $contato["con_assunto"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Mensagem</b><br>
                                            <?= $contato["con_mensagem"] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Data</b><br>
                                            <?= Util::getDateTimeToShow($contato["con_data_criacao"]); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($contato["con_status"] == 1) {
                                        ?>
                                        <tr>
                                            <td>
                                                <b>Respondido Em</b><br>
                                                <?= Util::getDateTimeToShow($contato["con_data_update"]); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr> 
                                        <td>
                                            <b>Resposta</b><br>
                                            <?php if (is_array($contato) && isset($contato["con_id"])) { ?>
                                                <input type="hidden" name="id" value="<?= $contato != null ? $contato["con_id"] : "" ?>">
                                            <?php } ?>
                                            <div class="form-group" style="margin-top: 8px;">
                                                <textarea class="form-control" name="resposta" id="txtResposta" rows="7" <?= $contato["con_status"] == 1 ? "disabled" : "" ?>><?= $contato != null ? $contato["con_resposta"] : "" ?></textarea>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btnOnClickDisable cursor-pointer" id="btnResposta" value="Atualizar" <?= $contato["con_status"] == 1 ? "disabled" : "" ?>>Responder</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </body>
</html>


<script>
    $(function () {

<?php if ($modalAddFlag) { ?>
            $("#modalAdd").modal("show");
<?php } ?>
    });
</script>
