<?php
require_once "./php/inc/basic.php";

$id = isset($_REQUEST["pub_id"]) ? $_REQUEST["pub_id"] : Session::get("pub")["pub_id"];
$pub = Pub::getPub($id);

Session::un_set("pub");
Session::set("pub", $pub);

if (isset($_REQUEST["action"])) {
    switch ($_REQUEST["action"]) {
        case "Add": {
                $modalAddFlag = true;
            } break;
        case "Edit": {
                $modalAddFlag = true;
                $id = Session::get("pub")["pub_id"];

                $pub = Pub::getPub($id);

                if (isset($_REQUEST["saved"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_SAVED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
                if (isset($_REQUEST["updated"])) {
                    $notify->set("Sucesso", SystemMessage::$SUCCESS_ITEM_UPDATED, Notify::NOTIFY_TYPE_SUCCESS, "modalAdd");
                }
            } break;
        case "Salvar": {

                $modalAddFlag = true;
                $pub["pub_titulo"] = $_REQUEST["titulo"];
                $pub["pub_descricao"] = $_REQUEST["descricao"];
                $pub["pub_website"] = $_REQUEST["website"];
                $pub["pub_dominio"] = $_REQUEST["dominio"];
                $pub["pub_email"] = $_REQUEST["email"];
                $pub["pub_facebook"] = $_REQUEST["facebook"];
                $pub["pub_twitter"] = $_REQUEST["twitter"];
                $pub["pub_instagram"] = $_REQUEST["instagram"];
                $pub["pub_youtube"] = $_REQUEST["youtube"];
                $pub["pub_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $pub["pub_texto_botao"] = $_REQUEST["texto_botao"];
                $pub["pub_link_botao"] = $_REQUEST["link_botao"];
                $pub["pub_exibir_botao"] = isset($_REQUEST["exibir_botao"]) ? 1 : 0;
                $pub["pub_logo"] = "";
                $logo = Upload::upload("logo", "./../img/repo/", true);

                if (Pub::validateSavePub($pub)) {
                    $id = Pub::savePub($pub, $logo);
                    if (is_numeric($id)) {
                        header("Location: gerenciar.pub.php?action=Edit&id={$id}&saved");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    }
                }
            } break;
        case "Atualizar": {
                $modalAddFlag = true;

                $pub["pub_id"] = Session::get("pub")["pub_id"];
                $pub["pub_titulo"] = $_REQUEST["titulo"];
                $pub["pub_descricao"] = $_REQUEST["descricao"];
                $pub["pub_website"] = $_REQUEST["website"];
                $pub["pub_dominio"] = $_REQUEST["dominio"];
                $pub["pub_email"] = $_REQUEST["email"];
                $pub["pub_facebook"] = $_REQUEST["facebook"];
                $pub["pub_twitter"] = $_REQUEST["twitter"];
                $pub["pub_instagram"] = $_REQUEST["instagram"];
                $pub["pub_youtube"] = $_REQUEST["youtube"];
                $pub["pub_status"] = isset($_REQUEST["status"]) ? 1 : 0;
                $pub["pub_texto_botao"] = $_REQUEST["texto_botao"];
                $pub["pub_link_botao"] = $_REQUEST["link_botao"];
                $pub["pub_exibir_botao"] = isset($_REQUEST["exibir_botao"]) ? 1 : 0;
                $pub["pub_logo"] = "";
                $pub["pub_banner"] = "";

                $logo = Upload::upload("logo", "./../img/repo/", true);
                $banner = Upload::upload("banner", "./../img/repo/", true);

                if (Pub::validateUpdatePub($pub)) {
                    $return = Pub::updatePub($pub, $logo, $banner);

                    if ($return) {
                        $id = $pub["pub_id"];
                        header("Location: gerenciar.pub.php?action=Edit&id={$id}&updated");
                    } else {
                        $notify->set("Erro: ", SystemMessage::$ERROR_CONTACT_ADMINISTRATOR, Notify::NOTIFY_TYPE_ERROR, "modalAdd");
                    };
                }
            } break;
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php"; ?>
        <link type="text/css" rel="stylesheet" href="./css/form.css" />
    </head>
    <body>
        <div class="container">
            <?php include "./php/inc/links.php"; ?>
            <?php
            $section_title = "<span class='fa fa-beer'></span> " . Session::get("pub")["pub_titulo"] . " - Editar Informações";
            include "./php/html/section.title.php";
            ?>
            <?php
            $notify->show("modalAdd");
            ?>
            <form method="POST" action="gerenciar.pub.php" enctype="multipart/form-data">

                <input type="hidden" name="action" value="">

                <div class="row">


                    <div class="col-md-4">

                        <div class="form-group">
                            <label for="filePubLogo">Pub Logo</label>
                            <div class="card" style="margin-bottom: 15px;">
                                <?php if (strlen($pub["pub_logo"]) > 0) { ?>
                                    <div class="row justify-content-md-center">
                                        <div class="col-lg-9">
                                            <div style="margin: 15px 15px 15px 15px;">
                                                <img class="card-img-top" src="./../img/repo/<?= $pub != null ? $pub["pub_logo"] : "" ?>"/> 
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="custom-file" style="display:block">
                                <input type="file" id="txtLogo" name="logo" class="custom-file-input">
                                <span class="custom-file-control"><?= $pub != null ? $pub["pub_logo"] : "" ?></span>
                            </label>
                        </div>

                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="status" <?= $pub != null ? ($pub["pub_status"] == 1 ? "checked" : "") : "checked" ?>>
                                Pub Ativa
                            </label>
                        </div>


                    </div>                 


                    <div class="col-md-8">

                        <?php if (is_array($pub) && isset($pub["pub_id"])) { ?>
                            <input type="hidden" name="id" value="<?= $pub != null ? $pub["pub_id"] : "" ?>">
                        <?php } ?>

                        <div class="form-group">
                            <label for="txtWebsite">Website</label>
                            <input type="text" class="form-control" name="website" id="txtWebsite" placeholder="" value="<?= $pub != null ? $pub["pub_website"] : "" ?>">
                        </div>

                        <div class="form-group">
                            <label for="txtDominio">Dominio</label>
                            <input type="text" class="form-control" name="dominio" id="txtDominio" placeholder="" value="<?= $pub != null ? $pub["pub_dominio"] : "" ?>">
                        </div>

                        <div class="form-group">
                            <label for="txtEmail">Email Address</label>
                            <input type="text" class="form-control" name="email" id="txtEmail" placeholder="" value="<?= $pub != null ? $pub["pub_email"] : "" ?>">
                        </div>

                        <div class="form-group">
                            <label for="txtFacebook">Facebook</label>
                            <div class="input-group">
                                <span class="input-group-addon fa fa-facebook" id="facebook"></span>
                                <input type="text" class="form-control" placeholder="" name="facebook" id="txtFacebook" aria-describedby="facebook" value="<?= $pub != null ? $pub["pub_facebook"] : "" ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtTwitter">Twitter</label>
                            <div class="input-group">
                                <span class="input-group-addon fa fa-twitter" id="twitter"></span>
                                <input type="text" class="form-control" placeholder="" name="twitter" id="txtTwitter" aria-describedby="twitter" value="<?= $pub != null ? $pub["pub_twitter"] : "" ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtInstagram">Instagram</label>
                            <div class="input-group">
                                <span class="input-group-addon fa fa-instagram" id="instagram"></span>
                                <input type="text" class="form-control" placeholder="" name="instagram" id="txtInstagram" aria-describedby="instagram" value="<?= $pub != null ? $pub["pub_instagram"] : "" ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtYoutube">Youtube</label>
                            <div class="input-group">
                                <span class="input-group-addon fa fa-youtube" id="youtube"></span>
                                <input type="text" class="form-control" placeholder="" name="youtube" id="txtYoutube" aria-describedby="youtube" value="<?= $pub != null ? $pub["pub_youtube"] : "" ?>">
                            </div>
                        </div> 
                    </div>  
                </div>
                <div class="section-title">

                    <h5><span class='fa fa-globe'></span><b> Website</b></h5>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="fileWebsiteBanner">Website Banner</label>
                            <div class="card" style="margin-bottom: 15px;">
                                <?php if (strlen($pub["pub_banner"]) > 0) { ?>
                                    <div class="row justify-content-md-center">
                                        <div class="col-lg-12">
                                            <div style="margin: 15px 15px 15px 15px;">
                                                <img class="card-img-top" src="./../img/repo/<?= $pub != null ? $pub["pub_banner"] : "" ?>"/> 
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="custom-file" style="display:block">
                                <input type="file" id="txtBanner" name="banner" class="custom-file-input">
                                <span class="custom-file-control"><?= $pub != null ? $pub["pub_banner"] : "" ?></span>
                            </label>
                        </div>
                    </div>                 

                    <div class="col-md-8">
                        
                        <div class="form-group">
                            <label for="txtTitulo">Titulo</label>
                            <input type="text" class="form-control" name="titulo" id="txtTitulo" placeholder="" value="<?= $pub != null ? $pub["pub_titulo"] : "" ?>">
                        </div>

                        <div class="form-group">
                            <label for="txtDescricao">Descrição</label>
                            <textarea class="form-control" name="descricao" id="txtDescricao" rows="4"><?= $pub != null ? $pub["pub_descricao"] : "" ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="txtTextoBotao">Texto do Botão</label>
                            <input type="text" class="form-control" name="texto_botao" id="txtTextoBotao" value="<?= ($pub != null) ? $pub["pub_texto_botao"] : "" ?>" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="txtLinkBotao">Link do Botão</label>
                            <input type="text" class="form-control" name="link_botao" id="txtLinkBotao" value="<?= ($pub != null) ? $pub["pub_link_botao"] : "" ?>" autocomplete="off">
                        </div>

                        <div class="form-check">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="exibir_botao" <?= $pub != null ? ($pub["pub_exibir_botao"] == 1 ? "checked" : "") : "checked" ?>>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Exibir Botão</span>
                            </label>
                        </div>

                        <div class="row" style="margin-bottom: 30px">
                            <div class="col-md-12" >
                                <?php
                                $txtButton = !isset($pub["pub_id"]) ? "Salvar" : "Atualizar";
                                ?>
                                <label for="txtYoutube">&nbsp;</label>
                                <input type="submit" class="btn btn-success btnOnClickDisable cursor-pointer btn-block" id="btnSalvar" value="<?= $txtButton ?>">
                            </div>
                        </div>

                    </div>  
                </div>
            </form>
        </div>
    </body>
</html>

<script>
    $(function () {

        $('.custom-file-input').on('change', function () {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-control').addClass("selected").html(fileName);
        });
    })
</script>
