<?php
require_once "./php/inc/basic.php";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php include "./php/inc/head.php" ?>
        <link type="text/css" rel="stylesheet" href="css/metro.css" />
    </head>
    <body>

        <div class="container-fluid">

            <?php include "./php/inc/links.php" ?>
            
            <div class="row">

                <div class="col-lg-12 col-sm-12">
                    <div class="section-title">
                        <h5><span class="fa fa-home"></span> Home</h5>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="metro-box metro-box-4 BtnMetro">
                                <div class="metro-box-icon"><span class="fa fa-beer"></span></div>
                                <div class="metro-box-text"><a href="gerenciar.pubs.php">Pubs</a></div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-sm-4">
                            <div class="metro-box metro-box-4 BtnMetro">
                                <div class="metro-box-icon"><span class="fa fa-cogs"></span></div>
                                <div class="metro-box-text"><a href="index.php">Confirgurações</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                $pubs = Pub::getPubsByOwner();
                if (count($pubs) > 0) {
                    ?>

                    <div class="col-lg-12 col-sm-12">
                        <div class="section-title">
                            <h5><span class="fa fa-beer"></span> Pubs</h5>
                        </div>
                    </div>

                    <div class="col-lg-12 col-sm-12">
                        <div class="row">

                            <?php
                            for ($i = 0; $i < count($pubs); $i++) {
                                ?>

                                <div class="col-lg-3">
                                    <div class="card" style="margin-bottom: 15px;">
                                        <div class="row justify-content-md-center">
                                            <img class="card-img-top img-fluid" src="./../img/repo/<?= $pubs[$i]["pub_logo"] ?>" alt="" style="width: 50%;  margin-top: 15px; max-height: 100px; min-height: 100px;">
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title"><?= $pubs[$i]["pub_titulo"] ?></h4>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item"><span class="fa fa-globe" style="margin-right: 10px; text-align: center;"></span> <?= $pubs[$i]["pub_website"] ?></li>
                                            <li class="list-group-item"><span class="fa fa-envelope" style="margin-right: 10px; text-align: center;"></span> <?= $pubs[$i]["pub_email"] ?></li>
                                        </ul>
                                        <div class="card-body">
                                            <a href="gerenciar.dashboard.php?pub_id=<?= $pubs[$i]["pub_id"] ?>" class="btn btn-sm btn-primary btn-block"><span class="fa fa-th-large"></span> Dashboard</a>
                                            <a href="gerenciar.pub.php?pub_id=<?= $pubs[$i]["pub_id"] ?>" class="btn btn-sm btn-info btn-block"><span class="fa fa-cogs"></span> Configurações</a>
                                            <a href="<?= $pubs[$i]["pub_website"] ?>" class="btn btn-sm btn-dark btn-block"><span class="fa fa-external-link"></span> Website</a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </div>

        </div>
    </body>
</html>


<script>
    $(function () {

        $(".BtnMetro").click(function () {
            document.location.href = $(this).find("a").attr("href");
        });

    });
</script>
